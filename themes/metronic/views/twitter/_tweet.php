<?php
?>
<div class="form">
<?php echo CHtml::beginForm('twitter/tweet', 'post', array('id' => 'frmTweet')); ?>
    <div class="form-group">
        <?php echo CHtml::label('Text', 'tweet'); ?>
        <?php echo CHtml::textArea('tweet', (isset($username)) ? "@". $username : '', 
                $htmlOptions = array('class' => 'form-control', 'cols' => 40, 'rows' => 3, 'maxlength' => 140)); ?>
        <?= CHtml::hiddenField('tweet_id', $tweet_id) ?>
    </div>
    <p>You have <span id="counter">140</span> characters left</p>
    <div class="modal-footer">
        <button type="button" id="btnSubmit" class="btn blue">Save changes</button>
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
    </div>
<?php echo CHtml::endForm(); ?>
</div>
<?php
$js = <<<JS
    $('#tweet').simplyCountable();
    $("#btnSubmit").click(function(evt){
        $.ajax({
            method: "POST",
            url: Yii.app.createUrl('twitter/tweet'),
            data: $("#frmTweet").serialize()
        }).done(function(result){
            alert(result);
            $("#modalWindow").modal("hide");
        });
    });
JS;
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/js/jquery.simplyCountable.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('tweetModal', $js, CClientScript::POS_END);
?>