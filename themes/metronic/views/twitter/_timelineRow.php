<?php
echo "<ul class='fetched_tweets'>";
foreach($statuses as $status){
    ?>
    <li class="tweets_avatar">
        <div class="tweet_wrap">
            <div class="wdtf-user-card ltr">
                <img width="45px" height="45px" src="<?= $status['profile_image'] ?>" alt="Tweet Avatar" class="circular" />
                <div class="wdtf-screen-name">
                    <span class="screen_name"><?= $status['name'] ?></span><br />
                    <a href="https://twitter.com/<?= $status['screen_name'] ?>" target="_blank" dir="ltr"><?= $status['screen_name'] ?></a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="tweet_data">
            <?php echo $status['text']; ?>
        </div>
        <br/>
        <div class="clear"></div>
        <div class="times">
            <em>
                <a href="#" target="_blank" title="Follow <?php echo $status['name']; ?> on Twitter [Opens new window]">
                    <?= TwitterHelper::HumanReadableTime($status["created_at"]) ?>
                </a>
            </em>
        </div>
        <div class="tweets-intent-data">
        <?php 
        if($status['favourite_count'] != 0 || $status['retweet_count'] !=0 ){?>
            <span class="stats-narrow customisable-border">
                <span class="stats" data-scribe="component:stats">
                    <?php 
                    if($status['retweet_count'] != 0)
                    {?>
                        <a href="https://twitter.com/<?php echo $status['screen_name']; ?>/statuses/<?php echo $status['tweet_id']; ?>" title="View Tweet on Twitter" data-scribe="element:favorite_count" target="_blank">
                            <span class="stats-favorites">
                                <strong><?php echo $status['retweet_count'];?></strong> retweet<?php if($status['retweet_count']>1) echo 's'; ?>
                            </span>
                        </a>
                    <?php 
                    } ?>
                    <?php 
                    if($status['favourite_count'] != 0)
                    {?>
                        <a href="https://twitter.com/<?php echo $status['screen_name']; ?>/statuses/<?php echo $status['tweet_id']; ?>" title="View Tweet on Twitter" data-scribe="element:favorite_count" target="_blank">
                            <span class="stats-favorites">
                                <strong><?php echo $status['favourite_count'];?></strong> Favorite<?php if($status['favourite_count']>1) echo 's';?>
                            </span>
                        </a>
                    <?php 
                    }?>
                </span>
            </span>
            <div class="clear"></div>
            <div class="seperator_wpltf"></div>
            <?php 
            } ?>
            <ul class="tweet-actions " role="menu" >
                <li>
                    <input type="hidden" id="id<?= $status['tweet_id'] ?>" value="<?= $status['screen_name'] ?>" />
                    <a href="#" id="<?php echo $status['tweet_id']; ?>" data-lang="en" class="in-reply-to" title="Reply">
                        <span aria-hidden="true" data-icon="&#xf079;"></span>
                    </a>
                </li>
                <li>
                    <a id="<?php echo $status['tweet_id']; ?>" data-lang="en" class="retweet" title="Retweet">
                        <span aria-hidden="true" data-icon="&#xf112;"></span>
                    </a>
                </li>
                <li>
                    <a id="<?php echo $status['tweet_id']; ?>" data-lang="en" class="favorite" title="Favorite">
                        <span aria-hidden="true" data-icon="&#xf005;"></span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="clear"></div>
    </li>    
<?php 
} 
echo "</ul>";
?>

