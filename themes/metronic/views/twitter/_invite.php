<?php
?>
<div class="form">
<?php echo CHtml::beginForm('twitter/invite', 'post', array('id' => 'frmInvite')); ?>
    <div class="form-group">
        <?php echo CHtml::label("Your friend's email", 'email'); ?>
        <?php echo CHtml::textField('email', '', 
                $htmlOptions = array('class' => 'form-control')); ?>
    </div>
    <div class="modal-footer">
        <button type="button" id="btnSubmit" class="btn blue">Save</button>
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
    </div>
<?php echo CHtml::endForm(); ?>
</div>
<?php
$js = <<<JS
    $("#btnSubmit").click(function(evt){
        $.ajax({
            method: "POST",
            url: Yii.app.createUrl('twitter/invite'),
            data: $("#frmInvite").serialize()
        }).done(function(result){
            if(result.charAt(0) == "<")
            {
                var win = $("#modalWindow").modal("show");
                $(".modal-title").text("Invite your friend");
                win.find(".modal-body").html(result);
            }
            else
            {
                alert(result);
                $("#modalWindow").modal("hide");
                //window.location.reload();
            }
        });
    });
JS;
Yii::app()->clientScript->registerScript('inviteForm', $js, CClientScript::POS_END);
?>