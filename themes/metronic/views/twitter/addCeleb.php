<?php
?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'addCeleb-form',
  'htmlOptions' => array('style' => 'width: 400px; padding-top: 10px')
)); ?>
    <?php echo $form->errorSummary($model); ?>
	<div class="form-group">
		<?php echo $form->label($model, 'twitter_username'); ?>
		<?php echo $form->textField($model, 'twitter_username', array('class' => 'form-control')); ?>
	</div>
    <div class="form-group">
        <?php echo $form->label($model, 'country_id'); ?>
        <?php echo $form->dropDownList($model, 'country_id', CHtml::listData($countries, 'id', 'country_name'), 
                array('empty' => 'Select a country', 'class' => 'form-control')); ?>
    </div>
    <div class="form-group">
    	<?php echo $form->label($model, 'category_ids'); ?>
    	<?php echo $form->listBox($model, 'category_ids', CHtml::listData($categories, 'id', 'name'), 
    	array('size' => '8', 'multiple' => 'true', 'class' => 'form-control')); ?>
    </div>
    <div class="form-group">
    	<?php echo CHtml::submitButton('Save', array('name' => 'btnSaveCeleb', 'class' => 'btn blue')); ?>
  	</div>
<?php $this->endWidget(); ?>
</div>
