<?php
?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'frmUpdateConfig',
  'htmlOptions' => array('style' => 'padding-top: 10px')
)); ?>    
<?php //echo CHtml::beginForm('twitter/updateUserCountry', 'post', array('id' => 'frmUpdateCountry')); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="form-group">
        <?php echo $form->label($model, 'country_id'); ?>
        <?php echo $form->dropDownList($model, 'country_id', CHtml::listData($countries, 'id', 'country_name'), 
                array('options' => array($country_id => array('selected' => TRUE)), 'empty' => 'Select a country', 'class' => 'form-control')); ?>
    </div>
    <div class="form-group">
    	<?php echo $form->label($model, 'category_ids'); ?>
    	<?php echo $form->listBox($model, 'category_ids', CHtml::listData($categories, 'id', 'name'), 
    	array('options' => $currentCategories, 'size' => '8', 'multiple' => 'true', 'class' => 'form-control')); ?>
    </div>
    <div class="modal-footer">
        <button type="button" id="btnSubmit" class="btn blue">Save changes</button>
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
    </div>
<?php //echo CHtml::endForm(); ?>
<?php $this->endWidget(); ?>
</div>
<?php
$js = <<<JS
    $("#btnSubmit").click(function(evt){
        $.ajax({
            method: "POST",
            url: Yii.app.createUrl('twitter/configureCountry'),
            data: $("#frmUpdateConfig").serialize()
        }).done(function(result){
            if(result.charAt(0) == "<")
            {
                var win = $("#modalWindow").modal("show");
                $(".modal-title").text("Please select your country");
                win.find(".modal-body").html(result);
            }
            else
            {
                alert(result);
                window.location.reload();
            }
        });
    });
JS;
Yii::app()->clientScript->registerScript('cfgUpdateConfig', $js, CClientScript::POS_END);
?>