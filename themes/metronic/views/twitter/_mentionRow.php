<?php
/*$patterns = array( '@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)@', '/@([A-Za-z0-9_]{1,15})/' );
$replace = array( '<a href="$1">$1</a>', '<a href="http://twitter.com/$1">@$1</a>' );
*/
echo "<ul class='fetched_tweets'>";
foreach($mentions as $mention){
    //$result = preg_replace($patterns, $replace, $mention["text"]);
    ?>
    <li class="tweets_avatar">
        <div class="tweet_wrap">
            <div class="wdtf-user-card ltr">
                <img width="45px" height="45px" src="<?= $mention['profile_image'] ?>" alt="Tweet Avatar" class="circular" />
                <div class="wdtf-screen-name">
                    <span class="screen_name"><?= $mention['name'] ?></span><br />
                    <a href="https://twitter.com/<?= $mention['screen_name'] ?>" target="_blank" dir="ltr"><?= $mention['screen_name'] ?></a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="tweet_data">
            <?php echo $mention['text']; ?>
        </div>
        <br/>
        <div class="clear"></div>
        <div class="times">
            <em>
                <a href="#" target="_blank" title="Follow <?php echo $mention['name']; ?> on Twitter [Opens new window]">
                    <?= TwitterHelper::HumanReadableTime($mention["created_at"]) ?>
                </a>
            </em>
        </div>
        <div class="tweets-intent-data">
        <?php 
        if($mention['favourite_count'] != 0 || $mention['retweet_count'] !=0 ){?>
            <span class="stats-narrow customisable-border">
                <span class="stats" data-scribe="component:stats">
                    <?php 
                    if($mention['retweet_count'] != 0)
                    {?>
                        <a href="https://twitter.com/<?php echo $mention['screen_name']; ?>/statuses/<?php echo $mention['tweet_id']; ?>" title="View Tweet on Twitter" data-scribe="element:favorite_count" target="_blank">
                            <span class="stats-favorites">
                                <strong><?php echo $mention['retweet_count'];?></strong> retweet<?php if($mention['retweet_count']>1) echo 's'; ?>
                            </span>
                        </a>
                    <?php 
                    } ?>
                    <?php 
                    if($mention['favourite_count'] != 0)
                    {?>
                        <a href="https://twitter.com/<?php echo $mention['screen_name']; ?>/statuses/<?php echo $mention['tweet_id']; ?>" title="View Tweet on Twitter" data-scribe="element:favorite_count" target="_blank">
                            <span class="stats-favorites">
                                <strong><?php echo $mention['favourite_count'];?></strong> Favorite<?php if($mention['favourite_count'] > 1) echo 's';?>
                            </span>
                        </a>
                    <?php 
                    }?>
                </span>
            </span>
            <div class="clear"></div>
            <div class="seperator_wpltf"></div>
            <?php 
            } ?>
            <ul class="tweet-actions " role="menu" >
                <li>
                    <input type="hidden" id="id<?= $mention['tweet_id'] ?>" value="<?= $mention['screen_name'] ?>" />
                    <a href="#" id="<?php echo $mention['tweet_id']; ?>" data-lang="en" class="in-reply-to" title="Reply">
                        <span aria-hidden="true" data-icon="&#xf079;"></span>
                    </a>
                </li>
                <li>
                    <a id="<?php echo $mention['tweet_id']; ?>" data-lang="en" class="retweet" title="Retweet">
                        <span aria-hidden="true" data-icon="&#xf112;"></span>
                    </a>
                </li>
                <li>
                    <a id="<?php echo $mention['tweet_id']; ?>" data-lang="en" class="favorite" title="Favorite">
                        <span aria-hidden="true" data-icon="&#xf005;"></span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="clear"></div>
<?php 
} 
echo "</ul>";
?>

