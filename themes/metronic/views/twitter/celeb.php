<?php
?>
<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<div id="modalWindow" class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
<!--                        <div class="modal-footer">
                <button type="button" id="btnSubmit" class="btn blue">Save changes</button>
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>-->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="row" style="padding:5px 0px 5px 0px;">
    <div class="col-md-offset-6">
        <input type="hidden" name="user_id" id="user_id" value="<?= $celeb->twitter_user_id ?>" />
        <button type="button" id="btnFollow" class="btn blue">Follow</button>
    </div>
</div>
<div class="row">
    <div id="celebTweets">
    <?php
    foreach($latestTweets as $tweet)
    {
        ?>
        <div class="col-md-4">
            <div class="thumbnail">
                <div class="topNames">
                    <a class="thumb" target="_blank" href="xx">
                        <img src="<?= $celeb->profile_image_url ?>" />
                    </a>
                    <a class="name">
                        <?= $celeb->name ?>
                    </a>
                    <a class="username">
                        @<?= $celeb->screen_name ?>
                    </a>
                </div>
                <?php
                if(isset($tweet['media_url']))
                    echo '<img src="'.$tweet['media_url']. ":small".'" />';
                ?>
                <div class="caption">
                    <p><?= $tweet['text'] ?></p>
                </div>
                <div class="foot">
                    <a class="icon twitter" rel="nofollow" target="_blank" href="http://twitter.com"></a>
                    <a class="time" rel="nofollow" target="_blank" href="#">
                        <?= TwitterHelper::HumanReadableTime($tweet['created_at']) ?>
                    </a>
                    <span class="actions">
                        <input type="hidden" id="id<?= $tweet['tweet_id'] ?>" value="<?= $celeb['screen_name'] ?>" />
                        <a id="<?php echo $tweet['tweet_id']; ?>" class="in-reply-to" title="Reply to this tweet">
                            <span aria-hidden="true" data-icon="&#xf079;"></span>
                        </a>
                        <a id="<?php echo $tweet['tweet_id']; ?>" data-lang="en" class="retweet" title="Retweet this">
                            <span aria-hidden="true" data-icon="&#xf112;"></span>
                        </a>
                        <a id="<?php echo $tweet['tweet_id']; ?>" data-lang="en" class="favorite" title="Favorite this tweet">
                            <span aria-hidden="true" data-icon="&#xf005;"></span>
                        </a>
                    </span>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
    </div>
<?php
$js = <<<JS
    jQuery(window).load(function () {
        var gutter = parseInt(jQuery('.col-md-4').css('marginBottom'));
        var container = jQuery('#celebTweets');
        container.masonry({
            gutter: gutter,
            itemSelector: '.col-md-4',
            columnWidth: '.col-md-4'
	});

    });
JS;
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/js/masonry.pkgd.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('masonry', $js, CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(
        Yii::app()->request->baseUrl . '/js/DashboardHelper.js', CClientScript::POS_END);
?>
</div>