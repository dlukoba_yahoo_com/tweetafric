<div class="page-container">
    <div class="page-content">
        <div class="container">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div id="modalWindow" class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
<!--                        <div class="modal-footer">
                            <button type="button" id="btnSubmit" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>-->
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="row margin-top-10">
                <div class="col-md-12">
                    <!-- BEGIN PROFILE SIDEBAR -->
                    <div class="profile-sidebar" style="width: 250px;">
                        <!-- PORTLET MAIN -->
                        <div class="portlet light profile-sidebar-portlet">
                            <!-- SIDEBAR USERPIC -->
                            <div class="profile-userpic">
                                <img src="<?php echo Yii::app()->session['profile_image_url']; ?>" class="img-responsive" alt="<?php echo Yii::app()->session['twitter_name']; ?>">
                            </div>
                            <!-- END SIDEBAR USERPIC -->
                            <!-- SIDEBAR USER TITLE -->
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name">
                                         <?php echo Yii::app()->session['twitter_name']; ?>
                                </div>
                                <div class="profile-usertitle-job">
                                    <?php echo $userdata->location; ?>
                                </div>
                            </div>
                            <!-- END SIDEBAR USER TITLE -->
                            <!-- SIDEBAR BUTTONS -->
                            <div class="profile-userbuttons">
                                <button type="button" id="btnTweet" class="btn btn-circle btn-danger btn-sm">Tweet</button>
                            </div>
                            <!-- END SIDEBAR BUTTONS -->
                            <!-- SIDEBAR MENU -->
                            <div class="profile-usermenu">
                                <ul class="nav">
<!--                                    <li>
                                            <a href="extra_profile.html">
                                            <i class="icon-home"></i>
                                            Personal Page </a>
                                    </li>-->
                                    <li class="active">
                                        <a href="<?php echo $this->createAbsoluteUrl("/twitter/myWall"); ?>">
                                            <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                            My Wall</a>
                                    </li>
                                    <li>
                                        <a id="account-settings" href="#">
                                        <i class="icon-settings"></i>
                                        Account Settings </a>
                                    </li>
<!--                                    <li>
                                            <a href="page_todo.html" target="_blank">
                                            <i class="icon-check"></i>
                                            Communication Wall </a>
                                    </li>-->
                                </ul>
                            </div>
                            <!-- END MENU -->
                    </div>
                    </div>
                    <!-- BEGIN PROFILE CONTENT -->
                    <div class="profile-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div id='loading' style="display: none;"><img src="<?php echo Yii::app()->baseUrl; ?>/images/loading.png"/></div>
                                <?php
                                if(count($categories) === 0)
                                {
                                    ?>
                                    <div class="col-md-11">
                                        <div class="alert alert-info" role="alert">
                                            There are no celebs configured for you that match your interests
                                        </div>
                                    </div>
                                <?php
                                }
                                else
                                {
                                    foreach ($categories as $key => $category) {
                                    ?>
                                    <div class="holder">
                                        <div class="copy orange BoxTitle"><?= $category['name']; ?></div>
                                        <div class="contentBox">
                                            <?php
                                            foreach ($most_followed_users_in_categories as $user)
                                            {
                                                if($user['category_id'] != $key)
                                                    continue;
                                                
                                                ?>
                                                <div class="userpluscount style2" style="width:100%; float: left;">
                                                    <a href="<?= Yii::app()->createUrl('twitter/celeb', array('id' => $user['screen_name'])) ?>">
                                                        <img class="img-rounded" width="30px" height="30px" src="<?php echo $user['profile_image_url']; ?>" />
                                                        <?= $user['twitter_name']; ?>
                                                    </a>
                                                    <span class="follow">
                                                    <?php 
                                                    /*if(Yii::app()->user->id){
                                                        //option to follow
                                                        if($user->following == FALSE)
                                                        {
                                                            echo "<a class='lnkFollow' id='". $user->id ."'>Follow</a>";
                                                        }
                                                        //option to unfollow
                                                        if($user->following == TRUE)
                                                        {
                                                            echo "<a class='lnkUnFollow' id='". $user->id ."'>Un-follow</a>";
                                                        }
                                                    }*/
                                                    ?>
                                                    </span>
                                                    <span class="followers" title="number of followers"><?= Yii::app()->numberFormatter->formatDecimal($user['followers_count']); ?></span>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                }
                                    Yii::app()->clientScript->registerScriptFile(
                                    Yii::app()->request->baseUrl . '/js/helpers.js', CClientScript::POS_END);
                                    Yii::app()->clientScript->registerScriptFile(
                                    Yii::app()->request->baseUrl . '/js/DashboardHelper.js', CClientScript::POS_END);
                                    ?>
                            </div>
                        </div>		
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>