<!DOCTYPE html>
<html>
    <head>
        <script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
        <script src="http://www.vibeink.net/assets/js/jquery.maximage.min.js" type="text/javascript"></script>
        <script src="http://www.vibeink.net/assets/js/jquery.cycle.all.js" type="text/javascript"></script>
        
         <link rel="stylesheet" href="http://www.vibeink.net/assets/css/jquery.maximage.css" type="text/css">
         <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

         <link href='http://fonts.googleapis.com/css?family=Share+Tech' rel='stylesheet' type='text/css'>
         <link href='http://fonts.googleapis.com/css?family=Carme' rel='stylesheet' type='text/css'>
         <link href='http://fonts.googleapis.com/css?family=Anaheim' rel='stylesheet' type='text/css'>
         <link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
         <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>

         <style type="text/css">

             p{
                font-size: 18px;
                font-family:  'Open Sans';
            }

             li{
                 font-family: Anaheim;
                 font-size: 20px;
              }

            #navigation{
/*                 background-color: #1F1716;*/
/*                 border-bottom: 3px solid #2b669a;*/
            }
            #text-blue{
                color: #F58635;
                font-family: Carme;
            }
            #text-black{
                color: #1F1716;
            }
            #blue-border{
                 border-color:#F58635;
            }
            #blue-btn{
                 background-color:#F58635;
                 border: 0;
            }
            #menu-col{
                 color:#FFF;
/*                 color:#F58635;*/
            }
            
            #more_details{
                 background-color: #1F1716;
                 border: 1px solid #c12e2a;
            }
            #login_panel{
                 border: 1px solid #1F1716;
            }
            #login_panel_header{
                 background-color: #1F1716;
/*                 border: 1px solid #3e8f3e;*/
            }
            #links{
                 color: #F58635;
/*                 text-decoration: none;*/
            }
            .style1{
                 color: #c12e2a;
                 font-family: Voces;
                 font-style: normal;
                 font-size: 13px;
            }
            .style2{
                 color: #c12e2a;
                 font-family: Voces;
            }
            .style3{
                 color: #3b5998;
                 font-family: Voces;
                 font-style: normal;
                 font-size: 13px;
            }
            #img-article{
                 width: 300px;
                 height: 150px;
                 border-radius:5px;
                 
                 
            }
            #item{
                 padding-top: 3px;
            }
            #thumbnail-details{
                 border: 0;
            }

            .thumbnail {
              display: block;
              padding: 0px;
              line-height: 1;
              border: 0;
              -webkit-border-radius: 6px;
              -moz-border-radius: 6px;
              border-radius: 6px;
              -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
              -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
              box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
            }
            .thumbnail > img {
              display: block;
              -webkit-border-radius: 6px;
              -moz-border-radius: 6px;
              border-radius: 6px;
              max-width: 100%;
              margin-left: auto;
              margin-right: auto;
            }
            .thumbnail .caption {
              padding: 9px;
              border: 0;
            }

            .item {
              display: block;
              padding: 4px;
              line-height: 1;
              border: 0;
              -webkit-border-radius: 6px;
              -moz-border-radius: 6px;
              border-radius: 6px;
              -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
              -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
              box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
            }
            .item > img {
              display: block;
              -webkit-border-radius: 6px;
              -moz-border-radius: 6px;
              border-radius: 6px;
              max-width: 100%;
              margin-left: auto;
              margin-right: auto;
            }
            .item .caption {
              padding: 0;
            }

            .link{
                text-decoration: none;
                color: #1F1716;
                font-size: 16px;
                font-weight: normal;
            }

            .h5-style{
                color: #F58635;
                font-weight: bold;
                font-size: 20px;
                font-family: Oswald;
            }

</style>
</head>
    <body>
    <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-4">
              <p>&nbsp;</p>
          </div>
          <div class="col-lg-4 col-md-4">
              <p>&nbsp;</p>              
              <h4></h4>

              <div class="panel" id="blue-border">
                  <div class="panel-body">
                      <form id="login-form" action="<?php echo Yii::app()->request->baseUrl."/site/login"; ?>" method="post">
                          <h5>Email</h5>
                          <p><input type="text" name="username" placeholder="Enter your registered email id" required class="form-control"></p>
                          <h5>Password</h5>
                          <p><input type="password" name="password" placeholder="Enter password" required class="form-control"></p>
                          <p><button id="blue-btn" class="btn btn-primary btn-sm">Login</button> </p>
                           <p><a href="<?php echo Yii::app()->request->baseUrl.'/site/register';?>">Create Account</a></p>
<!--                           <p><a href="http://www.vibeink.net/password">Forgot Password?</a></p>-->
                      </form>
                      <p>&nbsp;</p>
                      <center>
			<?php $this->widget('application.modules.hybridauth.widgets.renderProviders'); ?>
                      </center>
                  </div>
              </div>
          </div>
          <div class="col-lg-4 col-md-4">
              <p>&nbsp;</p>
          </div>
         </div><!-- /.row -->
      </div>

        <script src="http://www.vibeink.net/assets/js/bootstrap.min.js " type="text/javascript"></script>
        <script type="text/javascript">
function equalHeight(group) {
    tallest = 0;
    group.each(function() {
        thisHeight = $(this).height();
        if(thisHeight > tallest) {
            tallest = thisHeight;
        }
    });
    group.each(function() { $(this).height(tallest); });
}

$(document).ready(function() {
    equalHeight($(".thumbnail-height"));
});

$(document).ready(function() {
    equalHeight($(".thumbnail-height-lg"));
});

$(document).ready(function() {
    equalHeight($(".title-height"));
});

</script>

<script type="text/javascript">
window.fbAsyncInit = function() {
//Initiallize the facebook using the facebook javascript sdk
FB.init({
appId:'1440009222899616',
cookie:true, // enable cookies to allow the server to access the session
status:true, // check login status
xfbml:true, // parse XFBML
oauth : true //enable Oauth
});
};
//Read the baseurl from the config.php file
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));
//Onclick for fb login
$('#facebook_login').click(function(e) {

    FB.login(function(response) {
    if(response.authResponse) {
    parent.location ='http://www.vibeink.net/fb/fblogin'; //redirect uri after closing the facebook popup
    }
},{scope: 'email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos'}); //permissions for facebook
});
</script>

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=1440009222899616";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

    </body>
</html>
