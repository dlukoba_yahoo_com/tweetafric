    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?php echo Yii::app()->request->baseUrl;?>">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Cookie Policy</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12">
            <h1>Cookie Policy</h1>
            <div class="content-page">
              <div class="row margin-bottom-30">
                <!-- BEGIN INFO BLOCK -->               
<!--                <div class="col-md-7">
                  <h2 class="no-top-space">Cookies</h2>
                  
                </div> -->
<p>We may obtain information about your general internet usage by using a cookie file which is stored on your browser, or the hard drive of your computer or the memory of your mobile device. They help us to improve our site and to deliver a better and more personalised service. Some of the cookies we use are essential for the site to operate, such as the cookies we use to identify you when you log in. If you register with us or if you continue to use our site, you agree to our use of cookies. You also give consent to the use of cookies if you sign in with your twitter account.</p>
<p> For more information, please visit <a href="www.allaboutcookies.org">www.allaboutcookies.org </a> to learn more about cookies.
                <!-- END INFO BLOCK -->   

              
	     </div>
            </div>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

