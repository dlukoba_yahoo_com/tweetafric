    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?php echo Yii::app()->request->baseUrl;?>">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Are You A VIP?</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12">
            <h1>Tweetafric is FREE and You Can Be VIP</h1>
            <div class="content-page">               
			<!-- BEGIN INFO BLOCK -->
              <div class="row margin-bottom-20">
                <div class="col-md-12">
                    <p>Millions of individuals and businesses in Africa and around the world are using Twitter and a large % are constantly looking to grow their Twitter following. It makes a lot of sense because increasing your following on twitter would mean:</p>
                    <ul>
                        <li><p>TWITTER FOLLOWERS CONTRIBUTE IN THE INCREMENT OF POPULARITY OF YOUR BUSINESS OR PERSON. YOU ARE TAKEN MORE SERIOUSLY WHEN YOU HAVE A LOT OF FOLLOWERS. THEY ALSO PROMOTE YOUR VISIBILITY ACROSS SEARCH ENGINES SUCH AS GOOGLE, YAHOO, MSN ETC.</p></li>
                        <li><p>THE INCREASED NUMBER OF FOLLOWERS WILL FOR SURE CONTRIBUTE EXTENSIVELY IN BOOSTING UP YOUR TRAFFIC, SEARCH ENGINE VISIBILITY AND SALES.</p></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <p class="text-primary"><i>BUT THEN, ALL OF THIS WOULD BE WORTHLESS IF YOU DO NOT HAVE TARGETED FOLLOWERS. 
                    YOU MIGHT HAVE A MILLION FOLLOWERS AROUND THE WORLD, 
                    IF A LARGE MAJORITY OF THEM ARE NOT FROM YOUR REGION 
                    OR INTERESTED IN YOUR OFFERING, IT WOULD BE POINTLESS.
                        </i></p>
                </div>
              </div>
			  <!-- END INFO BLOCK --> 
                <div class="row margin-bottom-40">
				<!-- START FEATURES -->
				<div class="col-md-12">
                  <h4 class="no-top-space">WITH TWEETAFRIC, YOU ARE SURE OF GETTING TARGETED FOLLOWERS ALL OVER AFRICA</h4>
                  <!-- BEGIN LISTS -->
                  <div class="row front-lists-v1">
                    <div class="col-md-5">
					<b> <font color="green">Twitter Users Features :</font></b>
                      <ul class="list-unstyled margin-bottom-20">
                        <li><i class="fa fa-twitter"></i> Sign up with twitter, secure and protected.</li>
                        <li><i class="fa fa-twitter"></i> Follow other tweeps.</li>
                        <li><i class="fa fa-twitter"></i> Get followed.</li>
						<li><i class="fa fa-twitter"></i> Unfollow those who do not follow back.</li>
                        <li><i class="fa fa-twitter"></i> Scheduling of tweets to be sent at a later date</li>
                        <li><i class="fa fa-twitter"></i> Search for topics and interests. Save and group your interests.</li>
						<li><i class="fa fa-twitter"></i> Get a customized Social Wall.</li>
                      </ul>
                    </div>
                    <div class="col-md-7">
					<b> <font color="orange">VIP Users :</font></b>
                      <ul class="list-unstyled">
                        <li><i class="fa fa-twitter"></i> Start getting targeted followers the first five days absolutely FREE.</li>
                        <li><i class="fa fa-twitter"></i> Priority Exposure - Your profile listing will be prioritised in the directory so that others see you more often. </li>
                        <li><i class="fa fa-twitter"></i> Free Introductions - We'll give you free introductions so that your seeds are not always used when someone checks you out.</li>
						<li><i class="fa fa-twitter"></i> Follower Consistency - When we spot someone churning (following and unfollowing immediately) we get you more replacement followers. </li>
                        <li><i class="fa fa-twitter"></i> Priority Support - You can ask us a question any day of the week and we'll sort out any issues you have.</li>
                      </ul>
                    </div>
                  </div>
                  <!-- END LISTS -->
                </div>
				</div>
				<!-- END FEATURES -->
				<div class="row margin-bottom-40">
					<!-- START PRICING SECTION -->
					<div class="col-md-12" id="payment-options">
						<div class="row margin-bottom-40">
						<!-- Pricing -->
						<div class="col-md-3 planType" id="bronze">
						  <div class="pricing hover-effect">
							<div class="pricing-head">
							  <h3>Bronze
							  <span>
								 Get Twitter Followers Free
							  </span>
							  </h3>
							  <h4><i>$</i>0<i>.00</i>
							  <span>
								 For Life
							  </span>
							  </h4>
							</div>
							<ul class="pricing-content list-unstyled">
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok "></i>Get followers for free Daily
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-remove"></i>Add 500 followers Immediately
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-remove"></i>Get followers on Autopilot
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-remove"></i>Get followers by Country/Interest
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-remove"></i>Affservice
							  </li>
							</ul>
							<div class="pricing-footer">
							  <p>
								 This is the default plan that all new signups take up
							  </p>
							  <a href="payment/try" class="btn btn-primary">
								 Its Free! Try it <i class="m-icon-swapright m-icon-white"></i>
							  </a>
							</div>
						  </div>
						</div>
						<div class="col-md-3 planType" id="silver">
						  <div class="pricing hover-effect">
							<div class="pricing-head">
							  <h3>Silver
							  <span>
								 Become VIP Today: Get targeted Followers
							  </span>
							  </h3>
							  <h4><i><s>$19.99</s></i>&nbsp;&nbsp;<i>$</i>14<i>.99</i>
							  <span>
								 Monthly
							  </span>
							  </h4>
							</div>
							<ul class="pricing-content list-unstyled">
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Get followers for free Daily
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Add 500 followers Immediately
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Get followers on Autopilot
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Get followers by Country/Interest
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Affservice
							  </li>
							</ul>
							<div class="pricing-footer">
							  <p>
								 Get targeted Followers.
							  </p>
                                                          <a href="<?php echo Yii::app()->baseUrl ?>/payment/checkout/2" class="btn btn-primary">
								 Buy now <i class="m-icon-swapright m-icon-white"></i>
							  </a>
							</div>
						  </div>
						</div>
						<div class="col-md-3 planType" id="gold">
						  <div class="pricing pricing-active hover-effect">
							<div class="pricing-head pricing-head-active">
							  <h3>Gold
							  <span>
								 Become VIP Today: Get targeted Followers.
							  </span>
							  </h3>
							  <h4><i><s>$44.97</s></i>&nbsp;&nbsp;<i>$</i>35<i>.99</i>
							  <span>
								 3 Months
							  </span>
							  </h4>
							</div>
							<ul class="pricing-content list-unstyled">
							   <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Get followers for free Daily
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Add 500 followers Immediately
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Get followers on Autopilot
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Get followers by Country/Interest
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Affservice
							  </li>
							</ul>
							<div class="pricing-footer">
							  <p>
								 Get targeted Followers.
							  </p>
							  <a href="<?php echo Yii::app()->baseUrl ?>/payment/checkout/1" class="btn btn-primary">
								 Buy now <i class="m-icon-swapright m-icon-white"></i>
							  </a>
							</div>
						  </div>
						</div>
						<div class="col-md-3 planType" id="platinum">
						  <div class="pricing hover-effect">
							<div class="pricing-head">
							  <h3>Platinum
							  <span>
								 Become VIP Today: Get targeted Followers.
							  </span>
							  </h3>
							  <h4><i><s>$179.88</s></i>&nbsp;&nbsp;<i>$</i>99<i>.00</i>
							  <span>
								 Per Year
							  </span>
							  </h4>
							</div>
							<ul class="pricing-content list-unstyled">
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Get followers for free Daily
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Add 500 followers Immediately
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Get followers on Autopilot
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Get followers by Country/Interest
							  </li>
							  <li>
								 <i class="glyphicon glyphicon glyphicon-ok"></i>Affservice
							  </li>
							</ul>
							<div class="pricing-footer">
							  <p>
								 Get targeted Followers.
							  </p>
							  <a href="<?php echo Yii::app()->baseUrl ?>/payment/checkout/3" class="btn btn-primary">
								 Buy now <i class="m-icon-swapright m-icon-white"></i>
							  </a>
							</div>
						  </div>
						</div>
						<!--//End Pricing -->
						</div>
					</div>
					<!-- END PRICING -->
					<!--<div class="col-md-12" align="center">
						<p>GET 1000 targeted followers for only $9.99. For a limited time only. 
					</div>
					<div class="col-md-12" align="center">
						<a class="caption lft btn green slide_btn slide_item_left" href=""
						data-x="30" data-y="290" data-speed="400" data-start="3000" data-easing="easeOutExpo">
						ONE TIME PURCHASE
						</a>
					</div>
					&nbsp; <br>
					<div class="col-md-12" align="center">
						<a class="caption lft btn green slide_btn slide_item_left" href=""
						data-x="30" data-y="290" data-speed="400" data-start="3000" data-easing="easeOutExpo">
						CONTACT US NOW<br/> IF YOU HAVE <br/>QUESTIONS <i class="fa fa-envelope-o"></i>
						</a>
					</div>-->
					<div class="col-md-12">
						<!--ADVERT HERE-->
					</div>
					</div>
					<!-- END PRICING LIST -->
            </div>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

<?php
$js = <<<JS
    $(document).ready(function(evt){
        //alert('dd')
    });
JS;
Yii::app()->clientScript->registerScript('paymentPlans', $js, CClientScript::POS_END);
?>
