    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?php echo Yii::app()->request->baseUrl;?>">Home</a></li>
            <li><a href="#">Features</a></li>
            <li class="active">All</li>
        </ul>
<div class="col-md-4">
<!-- BEGIN ORDERED LISTS PORTLET-->
          <div class="portlet box blue">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-gift"></i>Twitter users features
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;">
                </a>
              </div>
            </div>
            <div class="portlet-body">
              <ol>
                <li>
                   Sign up with twitter, secure and protected
                </li>
                <li>
                   Follow other tweeps
                </li>
                <li>
                   Get followed
                </li>
                <li>
                   Unfollow those who do not follow back
                </li>
                <li>
                   SCheduling of tweets to be sent at a later date
                </li>
                <li>
                   Search for topics and interests. Save and group your interestes.
                </li>
                <li>
                   Get a customized Social Wall
                </li>
              </ol>
            </div>
          </div>
          <!-- END ORDERED LISTS PORTLET-->


          <!-- BEGIN UNORDERED LISTS PORTLET-->
          <div class="portlet box red">
            <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-gift"></i>VIP users
              </div>
              <div class="tools">
                <a class="collapse" href="javascript:;">
                </a>
              </div>
            </div>
            <div class="portlet-body">
              <ul>
                <li>
                   Priority Exposure - Your profile listing will be prioritised in the directory so that others see you more often.
                </li>
                <li>
                   Free Introductions - We'll give you free introductions so that your seeds are not always used when someone checks you out.
                </li>
                <li>
                   Follower Consistency - When we spot someone churning (following and unfollowing immediately) we get you more replacement followers. 
                </li>
                <li>
                   Priority Support - You can ask us a question any day of the week and we'll sort out any issues you have.</p>
                </li>
              </ul>
            </div>
          </div>
<!-- END UNORDERED LISTS PORTLET-->
</div>
<!-- end the column -->
   </div>
</div>        
    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/respond.min.js"></script>
    <![endif]--> 
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>      
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/scripts/metronic.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/layout/scripts/layout.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            Metronic.init(); // init global framework features
            Layout.init();
            Layout.initTwitter();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
