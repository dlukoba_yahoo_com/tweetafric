    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?php echo Yii::app()->request->baseUrl;?>">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Privacy Policy</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12">
            <h1><span style="color: #990000;">Privacy Policy</span></h1></hr>
            <div class="content-page">
              <div class="row margin-bottom-30">
<?php function formatH4 ($heading){return '<h4><span style="color: #FF9900;"><b>'.$heading.'</b></span></h4>'; }?>    
<?php echo formatH4("Introduction"); ?>
<p>At Tweetafric.com we value your privacy and believe in safeguarding your information just like you do. We strive to ensure that any information provided by you to us is secure using modern day processes and technologies. This privacy policy is effective from the 1st of May 2015; it points out what information we collect from registered free and VIP users or unique visitors, including but not limited to what we will and will not do with such information.</p>
<p>It should be noted that this privacy policy does not cover the collection and use of information by third party websites or companies we are affiliated to directly or indirectly but have no control over, nor by individuals not employed by Tweetafric.com. If you happen to visit a web site through Tweetafric.com do take out time to review its privacy policy. </p>

<?php echo formatH4("Your Personally Identifiable Information");?>
<p>Disclosing your personally identifiable information to us is a thing of choice. However, if you decide not to disclose such information, it is in our right not to register you as a user or provide you with this outstanding service. For us to better serve you it is important that we store your information that relate to you in our database. This is an advantage to you as it would help us to effectively get you targeted followers and more. We do not sell or disclose your information to any third party without your approval unless we are required by law to do so. Information that can be used to identify you as a person is what we refer to as “Personally identifiable information. Examples are but not limited to:</p>
<ul>
<li>Your full name, username, email address, the company you represent, telephone number, billing address, twitter login ID, password
<li>Credit/debit card information (where applicable, as this may not be applicable if credit/debit card information is handled by a payment processor)
<li>Any account-preference information provided to us by you
<li>Your personal computer’s domain name and IP address, indicating the location of your computer or phone or tablet
<li>Your login session data, in order for our computer to communicate with yours while you are logged in
</ul>
<p>In a situation whereby you provide personal identifiable information to us, whether directly or through our business partners or resellers, we will:</p>
<p>Not rent or sell to third party if not permitted by you – although unless you opt out, we may use your information to provide information that we perceive would be meaningful to you, e.g. information about existing and new services and changes to our terms of use. We may also share information with trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also provide information by mail, telephone or directly through Tweetafric.com on third party products and services that we believe are relevant to you based on your interest and location. Non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>
<p>Employ commercially realistic safety measures to safeguard your information from unauthorized access, theft, loss, alteration, misuse and destruction</p>
<p>Not disclose or use the information unless: 
<ul>
<li>We suspect that you are involved in but not limited to the below stated activities including violating the law, our website User Agreement, the rights of third parties, or misusing Tweetafric and its related properties.
<li>For the protection and defense of the property or rights of Tweetafric and its related properties, or other users of Tweetafric and its related properties.
<li>For processing and fulfilling your VIP subscription order or to notify you of the status of your order at any point in time.
<li>To get you third party products and services that are perceived as relevant to you based on your interest and location.
<li>To process your reseller registration.
<li>To cooperate with the authorities in a situation whereby investigations are being embarked on with regards to a purported unlawful activity.
<li>To work with outside auditors who are in agreement to ensure the confidentiality of the information.
<li>In the event of an acquisition, merger, bankruptcy or the disposition or sale of all or some parts of the assets of Vibeink Entertainment Nig. LTD. 
<li>It is important to enforce our Terms of Use. 
</ul>

<?php echo formatH4("Other Information Collected"); ?>
<p>In other to better serve you, we may collect other information that might not be directly used as identification for you. E.g. your web browser type, the device you use to log into Tweetafric, your domain name and network IP address. Such information may be used collectively or individually, for business or market research, the technical administration of Tweetafric.com, to help us better serve you going forward. In summary, your information helps us to personalize your user experience, to improve Tweetafric.com, to periodically send you vital information and improve customer service.</p>
<?php echo formatH4("Cookies");?>
<p>At Tweetafric.com, we use “cookies” to gather and personal data on your login device, e.g. your personal computer. We may as well connect the data stored on your computer in cookies with personal information about particular individuals kept in our servers. In a situation whereby you set up your internet browser to block cookies, accessing all of Tweetafric features will be difficult. It is therefore important that you allow cookies from Tweetafric.com to get the best out of the application.</p>
<?php echo formatH4("Third Party Links");?>
<p>From time to time, at our discretion, we may offer third party products or services on Tweetafric.com. Such third party sites have separate and independent privacy policies. We are therefore no responsible or liable for the activities or content of third party linked sites. However, we welcome feedbacks on your experience with such sites as we seek to protect our integrity going forward.</p>
<?php echo formatH4("Data Storage"); ?>
<p>We may keep your data or information on servers run by third party hosting companies in contract with us.</p>
<?php echo formatH4("User Role in the Protection of Privacy"); ?>
<p>It is advised that you never share your login details with anyone.</p>
<p>Ensure that you log off Tweetafric.com when you are not active in the site.</p>
<p>Install and update your anti-virus software on a regular basis to safeguard against “malware”.</p>
<p>Use alphabets and numbers together as password and change your password often.</p>
<?php echo formatH4("Age Limit Notice"); ?>
<p>You must be at least 14 years old to use Tweetafric.com and all of its services. Users below the age of 18 are to provide a consent notice from a parent or guardian to be able to use our VIP service. Tweetafric.com does not knowingly register or collect information from children under 14. </p>
<?php echo formatH4("Terms of Us"); ?>
<p>Please take time out to visit our Terms of Use section instituting the disclaimer, use, and liability limitations governing the use of Tweetafric.com at <a href="www.tweetafric.com/terms.html">www.tweetafric.com/terms.html</a></p>
<?php echo formatH4("Your Consen"); ?>
<p>By using Tweetafric.com, you consent to our privacy policy.</p>
<?php echo formatH4("Privacy Policy Changes"); ?>
<p>This privacy policy is subject to change at any time as a result of legal compliance requirements or changes in our business model or practices. When changes occur, you will be notified of such change by email.</p>
<?php echo formatH4("Do You Have Questions, Comments or Suggestions?"); ?>
<p>In case you have questions about Tweetafric.com’s privacy policy, feel free to email us at <a href="support@tweetafric.com">support@tweetafric.com</a>, contact us via our contact form on our home page or visit our office at Ero House, Lekki. Lagos. You can also contact us if you have suggestions on how we can serve you better.</p>
<p>Thanks for making Tweetafric.com your choice platform for social media growth and development.</p>

			<!-- END INFO BLOCK -->   

              
	     </div>
            </div>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

