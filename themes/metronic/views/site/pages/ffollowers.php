    <div class="main">
      <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?php echo Yii::app()->request->baseUrl;?>">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Free Followers</li>
        </ul>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12">
 <!--            <h1>Get Free Followers</h1> -->
            <div class="content-page">
              <div class="row margin-bottom-30">
                <!-- BEGIN INFO BLOCK -->               
                <div class="col-md-7">
                  <h2 class="no-top-space">Why Tweetafric?</h2>
                  <p>Tweetafric is a new age social media integration system that helps you get targeted followers on Twitter. Tweetafric is live because of your social media desires. Yes, you have always wanted to grow your social media presence. You have always wanted to have more relevant followers on social networking sites like Twitter and Instagram. We recognize that. This platform will help you grow your online presence to a level where you can confidently call yourself a social media rockstar. Tweetafric is the best social network growth site out of Africa, currently serving over 14 African countries with a fast growing inter-continental reach.</p> 
                  <p>We provide:</p>
                  <!-- BEGIN LISTS -->
                  <div class="row front-lists-v1">
                    <div class="col-md-6">
                      <ul class="list-unstyled margin-bottom-20">
                        <li><i class="fa fa-check"></i> Free access</li>
                        <li><i class="fa fa-check"></i> A guarantee of followers for free and as a VIP</li>
                        <li><i class="fa fa-check"></i> Fast turn around</li>
                      </ul>
                    </div>
                    <div class="col-md-6">
                      <ul class="list-unstyled">
			<li><i class="fa fa-check"></i> Targeted followers every time</li>
                        <li><i class="fa fa-check"></i> Added value for your money for VIP</li>
                        <li><i class="fa fa-check"></i> 90% correctness on results returned </li>
                        <li><i class="fa fa-check"></i> No spamming</li>
                      </ul>
                    </div>
                  </div>
                  <!-- END LISTS -->
				   <a class="caption lft btn blue slide_btn slide_item_left" href="<?php echo Yii::app()->request->baseUrl.'/index.php/site/login';    ?>"
                  data-x="30" data-y="290" data-speed="400" data-start="3000" data-easing="easeOutExpo">
                  Add your twitter, free!<i class="fa fa-twitter"></i>
                </a>

                </div>
                <!-- END INFO BLOCK -->   
		<!-- BEGIN VIDEO ITEM -->            
                <div class="col-md-5 front-carousel">
                 <!-- BEGIN VIDEO -->   
                  <iframe height="270" allowfullscreen="" style="width:100%; border:0" src="http://player.vimeo.com/video/56974716?portrait=0" class="margin-bottom-10"></iframe>
                  <!-- END VIDEO -->  
		</div>
                <!-- END VIDEO SECTION -->
              </div>
                
              <!-- comment out the word cloud image
              <div class="row margin-bottom-40">
                <div class="col-md-7 col-sm-7">
			<img src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/Worditout-tweetafric1.png" alt="wordcloud"> 
				
		</div>
                !-->
              <div class="row">
                <!-- BEGIN PROGRESS BAR -->
                <div class="col-md-5 front-skills">

                  <h2 class="block">What we provide</h2>
                  <span>Data correctness 90%</span>
                  <div class="progress">
                    <div role="progressbar" class="progress-bar" style="width: 90%;"></div>
                  </div>
                  <span>Turnaround time 90%</span>
                  <div class="progress">
                    <div role="progressbar" class="progress-bar" style="width: 90%;"></div>
                  </div>
                  <span>security 100%</span>
                  <div class="progress">
                    <div role="progressbar" class="progress-bar" style="width: 100%;"></div>
                  </div>
				  <!-- BEGIN TESTIMONIALS -->
                <div class="col-md-13 testimonials-v1">
				  <h2>Clients Testimonials</h2>                
                  <div id="myCarousel1" class="carousel slide">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                      <div class="active item">
                        <blockquote><p>Tweetafric has helped me grow my followers.</p></blockquote>
                        <div class="carousel-info">
                          <img class="pull-left" src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/people/img1-small.jpg" alt="">
                          <div class="pull-left">
                            <span class="testimonials-name">John Doe</span>
                            <span class="testimonials-post">Musician</span>
                          </div>
                        </div>
                      </div>
                      <div class="item">
                        <blockquote><p>Raw denim you Mustache cliche tempor, williamsburg carles vegan helvetica probably haven't heard of them jean shorts austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</p></blockquote>
                        <div class="carousel-info">
                          <img class="pull-left" src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/people/img5-small.jpg" alt="">
                          <div class="pull-left">
                            <span class="testimonials-name">Kate Ford</span>
                            <span class="testimonials-post">Commercial Director</span>
                          </div>
                        </div>
                      </div>
                      <div class="item">
                        <blockquote><p>Reprehenderit butcher stache cliche tempor, williamsburg carles vegan helvetica.retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.</p></blockquote>
                        <div class="carousel-info">
                          <img class="pull-left" src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/people/img2-small.jpg" alt="">
                          <div class="pull-left">
                            <span class="testimonials-name">Jake Witson</span>
                            <span class="testimonials-post">Commercial Director</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- Carousel nav -->
                    <a class="left-btn" href="#myCarousel1" data-slide="prev"></a>
                    <a class="right-btn" href="#myCarousel1" data-slide="next"></a>
                  </div>
				  </div>
                <!-- END TESTIMONIALS --> 
                </div>                       
                <!-- END PROGRESS BAR -->
              </div>
              </div>
            </div>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>

