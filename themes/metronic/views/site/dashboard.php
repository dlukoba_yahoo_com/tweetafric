<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div id="modalWindow" class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
<!--                        <div class="modal-footer">
                            <button type="button" id="btnSubmit" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>-->
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl;?>">DashBoard</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li class="active">
                    Personal Wall for <?php echo Yii::app()->session['twitter_name'];?>
                </li>
                <!-- HANDLE LOGIN MESSAGING -->
                <?php if(Yii::app()->user->hasFlash('loginMessage')):
                                    $loginMessage = Yii::app()->user->getFlash('loginMessage'); 
                ?>
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span><?php echo $loginMessage; ?></span>
                </div>
                <?php endif ?>
                <!-- END OF HANDLE LOGIN MESSAGING -->
            </ul>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="row margin-top-10">
                <div class="col-md-12">
                    <!-- BEGIN PROFILE SIDEBAR -->
                    <div class="profile-sidebar" style="width: 250px;">
                        <!-- PORTLET MAIN -->
                        <div class="portlet light profile-sidebar-portlet">
                            <!-- SIDEBAR USERPIC -->
                            <div class="profile-userpic">
                                <img src="<?php echo Yii::app()->session['profile_image_url']; ?>" class="img-responsive" alt="<?php echo Yii::app()->session['twitter_name']; ?>">
                                <!--ii::app()->theme->baseUrl;?>/admin/pages/media/profile/profile_user.jpg"-->
                            </div>
                            <!-- END SIDEBAR USERPIC -->
                            <!-- SIDEBAR USER TITLE -->
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name">
                                    <?php echo Yii::app()->session['twitter_name']; ?>
                                </div>
                                <div class="profile-usertitle-job">
                                    <?php echo $userdata->location; ?>
                                </div>
                            </div>
                            <!-- END SIDEBAR USER TITLE -->
                            <!-- SIDEBAR BUTTONS -->
                            <div class="profile-userbuttons">
                                <button type="button" id="btnTweet" class="btn btn-circle btn-danger btn-sm">Tweet</button>
                                <button type="button" id="btnInvite" class="btn btn-circle btn-subscribe btn-sm">Invite</button>
                            </div>
                            <!-- END SIDEBAR BUTTONS -->
                            <!-- SIDEBAR MENU -->
                            <div class="profile-usermenu">
                                <ul class="nav">
<!--                                    <li class="active">
                                        <a href="extra_profile.html">
                                        <i class="icon-home"></i>
                                        Personal Page </a>
                                    </li>-->
                                    <li class="active">
                                        <a href="<?php echo $this->createAbsoluteUrl("/twitter/myWall"); ?>">
                                            <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                            My Wall</a>
                                    </li>
                                    <li>
                                        <a class="account-settings" href="#">
                                        <i class="icon-settings"></i>
                                        Account Settings </a>
                                    </li>
<!--                                    <li>
                                        <a href="page_todo.html" target="_blank">
                                        <i class="icon-check"></i>
                                        Communication Wall </a>
                                    </li>
                                    <li>
                                        <a href="extra_profile_help.html">
                                        <i class="icon-info"></i>
                                        Help Section </a>
                                    </li>-->
                                </ul>
                            </div>
                            <!-- END MENU -->
                        </div>
                        <!-- END PORTLET MAIN -->
                        <!-- PORTLET MAIN -->
                        <div class="portlet light">
                            <!-- STAT -->
                            <div class="row list-separated profile-stat">
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title">
                                        <?php echo $userdata->friends_count; ?>
                                    </div>
                                    <div class="uppercase profile-stat-text">
                                        Friends
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title">
                                        <?php echo $userdata->followers_count; ?>
                                    </div>
                                    <div class="uppercase profile-stat-text">
                                        Followers
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="uppercase profile-stat-title">
                                        <?php echo $userdata->statuses_count; ?>
                                    </div>
                                    <div class="uppercase profile-stat-text">
                                        Statuses
                                    </div>
                                </div>
                            </div>
                            <!-- END STAT -->
                            <div>
                                <h4 class="profile-desc-title">About <?php echo Yii::app()->session['twitter_name']; ?></h4>
                                <span class="profile-desc-text"> <?php echo $userdata->description; ?> </span>
                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-globe"></i>
                                    <a href="www.tweetafric.com/members/123456"><?php echo $userdata->url; ?></a>
                                </div>
                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-twitter"></i>
                                    <a href="http://www.twitter.com/<?php echo $userdata->screen_name; ?>/">@<?php echo Yii::app()->session['twitter_name']; ?></a>
                                </div>
                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-facebook"></i>
                                    <a href="http://www.facebook.com/kay.shya/"><?php echo $userdata->screen_name; ?></a>
                                </div>
                            </div>
                        </div>
                        <!-- END PORTLET MAIN -->
                    </div>
                    <!-- END BEGIN PROFILE SIDEBAR -->
                    <!-- BEGIN PROFILE CONTENT -->
                    <div class="profile-content">
                        <div class="row">
                            <div class="col-md-6">
                                <!-- BEGIN PORTLET -->
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption caption-md">
                                            <i class="icon-bar-chart theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Your Activity</span>
                                            <span class="caption-helper hide">weekly stats...</span>
                                        </div>
                                        <div class="actions">
                                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                <label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
                                                <input type="radio" name="options" class="toggle" id="option1">Today</label>
                                                <label class="btn btn-transparent grey-salsa btn-circle btn-sm">
                                                <input type="radio" name="options" class="toggle" id="option2">Week</label>
                                                <label class="btn btn-transparent grey-salsa btn-circle btn-sm">
                                                <input type="radio" name="options" class="toggle" id="option2">Month</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row number-stats margin-bottom-30">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-left">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <div id="sparkline_bar">
                                                        </div>
                                                    </div>
                                                    <div class="stat-number">
                                                        <div class="title">
                                                            Total
                                                        </div>
                                                        <div class="number">
                                                            246
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="stat-right">
                                                    <div class="stat-chart">
                                                        <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                                        <div id="sparkline_bar2">
                                                        </div>
                                                    </div>
                                                    <div class="stat-number">
                                                        <div class="title">
                                                            New
                                                        </div>
                                                        <div class="number">
                                                            719
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-scrollable table-scrollable-borderless">
                                            <table class="table table-hover table-light">
                                            <thead>
                                            <tr class="uppercase">
                                                <th colspan="2">
                                                    MEMBER
                                                </th>
                                                <th>
                                                    WORTH
                                                </th>
                                                <th>
                                                    FOLLOWERS
                                                </th>
                                                <th>
                                                    FOLLOWS
                                                </th>
                                                <th>
                                                    RATE
                                                </th>
                                            </tr>
                                            </thead>
                                            <tr>
                                                <td class="fit">
                                                    <img class="user-pic" src="<?php echo Yii::app()->theme->baseUrl;?>/admin/layout3/img/avatar4.jpg">
                                                </td>
                                                <td>
                                                    <a href="#" class="primary-link">Brain</a>
                                                </td>
                                                <td>
                                                    $345
                                                </td>
                                                <td>
                                                    45
                                                </td>
                                                <td>
                                                    124
                                                </td>
                                                <td>
                                                    <span class="bold theme-font">80%</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fit">
                                                        <img class="user-pic" src="<?php echo Yii::app()->theme->baseUrl;?>/admin/layout3/img/avatar5.jpg">
                                                </td>
                                                <td>
                                                        <a href="#" class="primary-link">Nick</a>
                                                </td>
                                                <td>
                                                         $560
                                                </td>
                                                <td>
                                                         12
                                                </td>
                                                <td>
                                                         24
                                                </td>
                                                <td>
                                                        <span class="bold theme-font">67%</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fit">
                                                        <img class="user-pic" src="<?php echo Yii::app()->theme->baseUrl;?>/admin/layout3/img/avatar6.jpg">
                                                </td>
                                                <td>
                                                        <a href="#" class="primary-link">Tim</a>
                                                </td>
                                                <td>
                                                         $1,345
                                                </td>
                                                <td>
                                                         450
                                                </td>
                                                <td>
                                                         46
                                                </td>
                                                <td>
                                                        <span class="bold theme-font">98%</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                    <td class="fit">
                                                            <img class="user-pic" src="<?php echo Yii::app()->theme->baseUrl;?>/admin/layout3/img/avatar7.jpg">
                                                    </td>
                                                    <td>
                                                            <a href="#" class="primary-link">Tom</a>
                                                    </td>
                                                    <td>
                                                             $645
                                                    </td>
                                                    <td>
                                                             50
                                                    </td>
                                                    <td>
                                                             89
                                                    </td>
                                                    <td>
                                                            <span class="bold theme-font">58%</span>
                                                    </td>
                                            </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PORTLET -->
                            </div>
                            <div class="col-md-6">
                                <!-- BEGIN PORTLET -->
                                <div class="portlet light">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Twitter Feed</span>
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1_1" data-toggle="tab">
                                                Tweets </a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_2" data-toggle="tab">
                                                Mentions </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portlet-body">
                                        <!--BEGIN TABS-->
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1_1">
                                                <div class="scroller" style="height: 320px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                                    <ul class="fetched_tweets light">
                                                    	
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab_1_2">
                                                <div class="scroller" style="height: 337px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                                    <ul class="fetched_tweets light">
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!--END TABS-->
                                    </div>
                                </div>
                                <!-- END PORTLET -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <!-- BEGIN PORTLET -->
                                <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption caption-md">
                                            <i class="icon-bar-chart theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Polls</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                            <div class="general-item-list">
<!--                                                <div class="item">
                                                    <div class="item-head">
                                                        <div class="item-details">
                                                            <img class="item-pic" src="<?php echo Yii::app()->theme->baseUrl;?>/admin/layout3/img/avatar4.jpg">
                                                            <a href="" class="item-name primary-link">Nick Larson</a>
                                                            <span class="item-label">3 hrs ago</span>
                                                        </div>
                                                        <span class="item-status"><span class="badge badge-empty badge-success"></span> Open</span>
                                                    </div>
                                                    <div class="item-body">
                                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                                    </div>
                                                </div>-->
                                                <?php $this->widget('application.modules.poll.components.EPoll'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PORTLET -->
                            </div>
                            <div class="col-md-6">
                                <!-- BEGIN PORTLET -->
                                <div class="portlet light tasks-widget">
                                <div class="portlet-title">
                                    <div class="caption caption-md">
                                        <i class="icon-bar-chart theme-font hide"></i>
                                        <span class="caption-subject font-blue-madison bold uppercase">Trending</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="task-content">
                                        <div class="scroller" style="height: 282px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                            <!-- START TASK LIST -->
                                            <ul class="task-list">
                                                
                                            </ul>
                                            <!-- END START TASK LIST -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET -->
                        </div>
                    </div>
                    </div>
                    <!-- END PROFILE CONTENT -->
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    <!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<?php
$countryIsSet = json_encode(($userdata->countryid == NULL) ? FALSE : TRUE);
$js = <<<JS
    var isCountrySetup = "{$countryIsSet}"
    if(isCountrySetup == "false")
        displayCountryUpdateForm();
JS;

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/js/DashboardHelper.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('countryModal', $js, CClientScript::POS_END);
?>