<?php
/*
<style type="text/css">
 body{
        font-family: "Open Sans" !important;
	text-align:justify;
	
}
*/
?>
</style>
    <!-- BEGIN SLIDER -->
    <div class="page-slider margin-bottom-40">
      <div class="fullwidthbanner-container revolution-slider">
        <div class="fullwidthabnner">
          <ul id="revolutionul">
            <!-- THE NEW SLIDE -->
            <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
              <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
              <img src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/revolutionslider/bg9.jpg" alt="">
              
              <div class="caption lft slide_title_white slide_item_left"
                data-x="30"
                data-y="90"
                data-speed="400"
                data-start="1500"
                data-easing="easeOutExpo">
                Explore the Power<br><span class="slide_title_white_bold">of Tweetafric</span>
              </div>
              <div class="caption lft slide_subtitle_white slide_item_left"
                data-x="87"
                data-y="245"
                data-speed="400"
                data-start="2000"
                data-easing="easeOutExpo">
                The most robust way to grow your twitter following.
              </div>
              <a class="caption lft btn blue slide_btn slide_item_left" href="<?php echo Yii::app()->request->baseUrl.'/index.php/site/login';?>"
                data-x="187"
                data-y="315"
                data-speed="400"
                data-start="3000"
                data-easing="easeOutExpo">
                Add your twitter, free!
              </a>                        
              <div class="caption lfb"
                data-x="640" 
                data-y="0" 
                data-speed="700" 
                data-start="1000" 
                data-easing="easeOutExpo">
                <img src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/revolutionslider/twitterfollowers_1-376x370.png" alt="Image 1">
              </div>
            </li>    

            <!-- THE FIRST SLIDE -->
            <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
              <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
              <img src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/revolutionslider/bg1.jpg" alt="">
                            
              <div class="caption lft slide_title slide_item_left"
                data-x="30"
                data-y="105"
                data-speed="400"
                data-start="1500"
                data-easing="easeOutExpo">
                Need followers at a snap? 
              </div>
              <div class="caption lft slide_subtitle slide_item_left"
                data-x="30"
                data-y="180"
                data-speed="400"
                data-start="2000"
                data-easing="easeOutExpo">
                Tweetafric is there for you for FREE!
              </div>
              <div class="caption lft slide_desc slide_item_left"
                data-x="30"
                data-y="220"
                data-speed="400"
                data-start="2500"
                data-easing="easeOutExpo">
                Tweetafric helps you to grow your audience<br>
		 on Twitter. We are a vibrant community <br>
		of twitter users. We are waiting to follow you..!
              </div>
              <a class="caption lft btn blue slide_btn slide_item_left" href="<?php echo Yii::app()->request->baseUrl.'/index.php/site/login';?>"
                data-x="30"
                data-y="290"
                data-speed="400"
                data-start="3000"
                data-easing="easeOutExpo">
                Add your twitter, free!
              </a>                        
              <div class="caption lfb"
                data-x="640" 
                data-y="55" 
                data-speed="700" 
                data-start="1000" 
                data-easing="easeOutExpo">
                <img src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/revolutionslider/man-winner.png" alt="Image 1">
              </div>
            </li>
<?php
/*
            <!-- THE SECOND SLIDE -->                        
            <!-- THE THIRD SLIDE -->
            <!-- THE FORTH SLIDE -->
*/
?>
                </ul>
                <div class="tp-bannertimer tp-bottom"></div>
            </div>
       </div>
    </div>
    <!-- END SLIDER -->

    <div class="main">
      <div class="container">
        <!-- BEGIN SERVICE BOX -->   
        <div class="row service-box margin-bottom-40">
          <div class="col-md-4 col-sm-4">
            <div class="service-box-heading">
              <em><i class="fa fa-location-arrow blue"></i></em>
              <span class=".allbabesneue">GET FREE FOLLOWERS</span>
            </div>
            <p>At tweetafric we don't sell followers, we promote you safely and responsibly to all our visitors and members. We're a directory of millions of people listed by country and interests. 
</p><p>
Adding yourself is quick and easy and will have people checking you out within minutes. We actively moderate the community and do our best to block fake and inappropriate users.</p>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="service-box-heading">
              <em><i class="fa fa-check red"></i></em>
              <span class=".allbabesneue">ROBUST WALL</span>
            </div>
            <p>When you add yourself to tweetafric you get your very own social wall that you can share with others. It shows all your recent updates from Twitter and Instagram in a stunning full page layout. 
<p></p>
You can choose which types of updates to show on your wall, and can remove individual updates too. These pages are great for showing others what you're all about, and why they should follow you..! 
</p><p>
You can also track your favorite pages for other users. We'll highlight when they've added updates so that you don't miss anything.</p>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="service-box-heading">
              <em><i class="fa fa-compress green"></i></em>
              <span class=".allbabesneue">AWESOME DIRECTORY</span>
            </div>
            <p>At the heart of tweetafric is our fully browseable interest-based directory. When you add yourself to tweetafric you are automatically added to the parts of the directory you choose. This makes it easy for others with similar interests to discover you.</p>
          </div>
        </div>
        <!-- END SERVICE BOX -->

        <!-- BEGIN BLOCKQUOTE BLOCK -->   
        <div class="row quote-v1 margin-bottom-30">
          <div class="col-md-9">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Amaranth" />

            <span class="allAmaranth">Tweetafric - The Most Robust twitter directory for African tweeps</span>
          </div>
          <div class="col-md-3 text-right">
            <a class="btn-transparent" href="#" target="_blank"><i class="fa fa-rocket margin-right-10"></i>Rate</a>
          </div>
        </div>
        <!-- END BLOCKQUOTE BLOCK -->

        <!-- BEGIN RECENT WORKS -->
        <!-- END RECENT WORKS -->

        <!-- BEGIN TABS AND TESTIMONIALS -->
        <div class="row mix-block margin-bottom-40">
          <!-- TABS -->
          <div class="col-md-7 tab-style-1">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab-1" data-toggle="tab"><span class="allAmaranth">Yes its free!</span> </a></li>
              <li><a href="#tab-2" data-toggle="tab"><span class="allAmaranth">Are You A VIP User?</span></a></li>
              <li><a href="#tab-3" data-toggle="tab"><span class="allAmaranth">How Tweetafric Works</span></a></li>
             <!--  <li><a href="#tab-4" data-toggle="tab">Disclaimer</a></li> -->
            </ul>
            <div class="tab-content">
              <div class="tab-pane row fade in active" id="tab-1">
                <div class="col-md-3 col-sm-3">
                  <a href="assets/temp/photos/img7.jpg" class="fancybox-button" title="Image Title" data-rel="fancybox-button">
                    <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/photos/img7.jpg" alt="">
                  </a>
                </div>
                <div class="col-md-9 col-sm-9">
                  <p class="margin-bottom-10">Tweetafric is completely free. There are no subscription fees or hidden charges that result from usage of the platform. We don't remove any features for non-paying members. If you want to promote yourself a little faster though, we offer paid options to help you reach more people. All purchases on tweetafric make you a VIP.</p>
                  <p><a class="more" href="#">Read more <i class="icon-angle-right"></i></a></p>
                </div>
              </div>
              <div class="tab-pane row fade" id="tab-2">
                <div class="col-md-9 col-sm-9">
                  <p>Whenever you make a purchase on tweetafric you become a VIP. The duration of the VIP period is shown on the package you choose. VIP's receive a number of benefits on the site: 
<ul>
<li>Priority Exposure - Your profile listing will be prioritised in the directory so that others see you more often.
<li>Free Introductions - We'll give you free introductions so that your seeds are not always used when someone checks you out.
<li>Follower Consistency - When we spot someone churning (following and unfollowing immediately) we get you more replacement followers.
<li>Priority Support - You can ask us a question any day of the week and we'll sort out any issues you have.
</ul>
                </div>
                <div class="col-md-3 col-sm-3">
                  <a href="assets/temp/photos/img10.jpg" class="fancybox-button" title="Image Title" data-rel="fancybox-button">
                    <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/photos/img10.jpg" alt="">
                  </a>
                </div>
              </div>
              <div class="tab-pane fade" id="tab-3">
                <p>Our policies are same as twitter. We enforce the limits applied when it comes to following and posting. We uses the Instagram™ API and Twitter™ API and is not endorsed or certified by Instagram or Twitter. All Instagram™ and Twitter™ logos and trademarks displayed on tweetafric are property of Instagram and Twitter respectively.</p>
              </div>
<!--              <div class="tab-pane fade" id="tab-4">
                <p>There are no refunds for payments made to twetafric.</p>
              </div> -->
            </div>
          </div>
          <!-- END TABS -->
        
          <!-- TESTIMONIALS -->
          <div class="col-md-5 testimonials-v1">
            <div id="myCarousel" class="carousel slide">
              <!-- Carousel items -->
              <div class="carousel-inner">
                <div class="active item">
                  <blockquote><p>Testimonial 1.</p></blockquote>
                  <div class="carousel-info">
                    <img class="pull-left" src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/people/img1-small.jpg" alt="">
                    <div class="pull-left">
                      <span class="testimonials-name">John Doe</span>
                      <span class="testimonials-post">Celebrity</span>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <blockquote><p>Testimonial 2</p></blockquote>
                  <div class="carousel-info">
                    <img class="pull-left" src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/people/img5-small.jpg" alt="">
                    <div class="pull-left">
                      <span class="testimonials-name">Jane Doe</span>
                      <span class="testimonials-post">Student</span>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <blockquote><p>Testimonial 3.</p></blockquote>
                  <div class="carousel-info">
                    <img class="pull-left" src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/img/people/img2-small.jpg" alt="">
                    <div class="pull-left">
                      <span class="testimonials-name">Jack Doe</span>
                      <span class="testimonials-post">Musician</span>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Carousel nav -->
              <a class="left-btn" href="#myCarousel" data-slide="prev"></a>
              <a class="right-btn" href="#myCarousel" data-slide="next"></a>
            </div>
          </div>
          <!-- END TESTIMONIALS -->
        <!-- END TABS AND TESTIMONIALS -->

        </div>                
        <!-- BEGIN STEPS -->
        <div class="row margin-bottom-40 front-steps-wrapper front-steps-count-3">
          <div class="col-md-4 col-sm-4 front-step-col">
            <div class="front-step front-step1">
              <h2><a hrf="#"><font color="white">Sign up</font></a></h2>
              <p>Sign up free with your twitter account.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 front-step-col">
            <div class="front-step front-step2">
              <h2><a href="<?php echo Yii::app()->request->baseUrl.'/site/page?view=browse';?>"><font color="white">Grow Your Social Circles</font></a></h2>
              <p>Build your circles based on country and interests.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 front-step-col">
            <div class="front-step front-step3">
              <h2><a href="<?php echo Yii::app()->request->baseUrl.'/site/page?view=ruvip';?>"><font color="white">Follow and Get followed</font></a></h2>
              <p>Follow and get followed from our directory of millions of people listed by country and interests.</p>
            </div>
          </div>
        </div>
        <!-- END STEPS -->

        <!-- BEGIN CLIENTS -->
        <!-- END CLIENTS -->
      </div>
    </div>

    <!-- BEGIN PRE-FOOTER -->
    <div class="pre-footer">
      <div class="container">
        <div class="row">
          <!-- BEGIN BOTTOM ABOUT BLOCK -->
          <div class="col-md-4 col-sm-6 pre-footer-col">
            <h2>About Tweetafric</h2>
<p> Tweetafric.com is a free social media growth application that helps individuals and businesses expand their social media reach through strategic analysis. This intelligent application helps to boost your following on Twitter by targeting and linking accounts that have the same interest as you and are in proximity to you. Having targeted followers on any social media platform is essential for growth and development depending on your set out social media goals. Our goal is to help you achieve your social media goals. </p>
<!-- PHOTO STREAM WAS HERE -->
          </div>
          <!-- END BOTTOM ABOUT BLOCK -->

          <!-- BEGIN BOTTOM CONTACTS -->
          <div class="col-md-4 col-sm-6 pre-footer-col">
            <h2>Our Base</h2>
            <address class="margin-bottom-40">
              EROS HOUSE,<br> 
	      Adjacent MAYFAIR GARDENS,<br>
	      Lekki-Epe Expressway,<br>
              Lagos, Nigeria<br><br>
              Phone: +2349090406140<br>
              Fax: xxx xxx xxx<br>
              Email: <a href="mailto:info@tweetafric.com">info@tweetafric.com</a><br>
              Skype: <a href="skype:tweetafric">tweetafric</a>
            </address>

<!--            <div class="pre-footer-subscribe-box pre-footer-subscribe-box-vertical">
              <h2>Newsletter</h2>
              <p>Subscribe to our newsletter and stay up to date with the latest news and deals!</p>
              <form action="#">
                <div class="input-group">
                  <input type="text" placeholder="youremail@mail.com" class="form-control">
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">Subscribe</button>
                  </span>
                </div>
              </form>
            </div>
-->    
      </div>
          <!-- END BOTTOM CONTACTS -->

          <!-- BEGIN TWITTER BLOCK --> 
          <div class="col-md-4 col-sm-6 pre-footer-col">
            <h2 class="margin-bottom-0">Latest Tweets for @kaylayshya</h2>
 <!--           <a class="twitter-timeline" href="https://twitter.com/twitterapi" data-tweet-limit="2" data-theme="dark" data-link-color="#57C8EB" data-widget-id="585048252720013312" data-chrome="noheader nofooter noscrollbar noborders transparent">Loading tweets by @kaylayshya...</a>
-->
<a class="twitter-timeline" href="https://twitter.com/kaylayshya" data-widget-id="585048252720013312">Tweets by @kaylayshya</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          </div>
          <!-- END TWITTER BLOCK -->
        </div>
      </div>
    </div>
    <!-- END PRE-FOOTER -->

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>      
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/layout/scripts/back-to-top.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->

    <!-- BEGIN RevolutionSlider -->
  
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.plugins.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script> 
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/slider-revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script> 
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/scripts/revo-slider-init.js" type="text/javascript"></script>
    <!-- END RevolutionSlider -->

    <script src="<?php echo Yii::app()->theme->baseUrl;?>/frontend/layout/scripts/layout.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();    
            Layout.initOWL();
            RevosliderInit.initRevoSlider();
            Layout.initTwitter();
            Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
            Layout.initNavScrolling();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
