
	<!-- BEGIN HEADER MENU -->
	<div class="page-header-menu">
		<div class="container">
			<!-- BEGIN HEADER SEARCH BOX -->
<!--			<form class="search-form" action="extra_search.html" method="GET">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search" name="query">
					<span class="input-group-btn">
					<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
					</span>
				</div>
			</form>
-->
			<!-- END HEADER SEARCH BOX -->
			<!-- BEGIN MEGA MENU -->
			<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
			<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                        <div class="hor-menu">
                            <div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active">
						<a href="<?php echo Yii::app()->request->baseUrl;?>">Dashboard</a>
					</li>
                                        <li class="menu-dropdown mega-menu-dropdown ">
                                            <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
						My Menu <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu" style="min-width: 710px">
                                                <li>
                                                    <div class="mega-menu-content">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <ul class="mega-menu-submenu">
                                                                    <li>
                                                                        <a href="#" class="iconify account-settings"><i class="icon-link"></i>Account Settings </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="<?php echo Yii::app()->urlManager->createUrl('twitter/myWall'); ?>" class="iconify"><i class="icon-settings"></i>My Wall </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" class="iconify"><i class="icon-link"></i>Follow Accounts near you</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="<?php echo Yii::app()->urlManager->createUrl('poll/poll/create', array('id' => 'poll')); ?>" class="iconify"><i class="icon-settings"></i>Create Polls</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="active">
                                            <a href="<?php echo Yii::app()->baseUrl;?>/site/page?view=ruvip">Are you a VIP?</a>
					</li>
<!--					<li class="menu-dropdown mega-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
						My Menu <i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu" style="min-width: 710px">
							<li>
								<div class="mega-menu-content">
									<div class="row">
										<div class="col-md-4">
											<ul class="mega-menu-submenu">
												<li>
													<h3>VIP</h3>
												</li>
												<li>
													<a href="#l" class="iconify">
													<i class="icon-home"></i>
													Become a VIP </a>
												</li>
												<li>
													<a href="ecommerce_orders.html" class="iconify">
													<i class="icon-basket"></i>
													Check my Status </a>
												</li>
											</ul>
										</div>
										<div class="col-md-4">
											<ul class="mega-menu-submenu">
												<li>
													<h3>My Activity</h3>
												</li>
												<li>
													<a href="#" class="iconify">
													<i class="icon-cursor-move"></i>
													Followers </a>
												</li>
												<li>
													<a href="#" class="iconify">
													<i class="icon-pin"></i>
													Who am following </a>
												</li>
											</ul>
										</div>
										<div class="col-md-4">
											<ul class="mega-menu-submenu">
												<li>
													<h3>Interests</h3>
												</li>
												<li>
													<a href="#" class="iconify">
													<i class="icon-speech"></i>
													View Lists </a>
												</li>
												<li>
													<a href="#" class="iconify">
													<i class="icon-link"></i>
													My Interests </a>
												</li>
												<li>
													<a href="#" class="iconify">
													<i class="icon-settings"></i>
													Celebs </a>
												</li>
												<li>
													<a href="#" class="iconify">
													<i class="icon-globe"></i>
													People Near Me </a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>-->
				</ul>
                                <form class="navbar-form pull-right" role="search" id="frmSearchCeleb">
                                    <div class="form-group">
                                        <input type="text" name="searchQ" id="searchQ" class="form-control" placeholder="Search a celeb" />
                                        <div id="suggesstion-box"></div>
                                    </div>
                                    <button type="submit" class="btn btn-default">Go</button>
                                </form>
                            </div>
			</div>
			<!-- END MEGA MENU -->
		</div>
	</div>
	<!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
