<!-- Require the header -->
<?php require_once('header.php')?>

<!-- Require the navigation -->
<?php require_once('navigation.php')?>

<!-- Include content pages -->
<?php
/*
<style type="text/css">
 body{
        font-family: "Open Sans" !important;
        text-align:justify;

}
*/
?>
<?php echo $content; ?>

<!-- Require the footer -->
<?php require_once('tpl_footer.php')?>
