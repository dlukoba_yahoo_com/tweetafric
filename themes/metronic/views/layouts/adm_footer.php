<!-- BEGIN PRE-FOOTER -->
<div class="page-prefooter">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12 footer-block">
				<h2>About</h2>
				<p>
                                    This is yout tweetafric personal page. Your friends can access it via <?php echo Yii::app()->getBaseUrl(true) . '/site/index/' . Yii::app()->session['access_token']['screen_name']; ?>
				</p>
			</div>
			<div class="col-md-3 col-sm-6 col-xs12 footer-block">
				<h2>Subscribe Email</h2>
				<div class="subscribe-form">
					<form action="javascript:;">
						<div class="input-group">
							<input type="text" placeholder="mail@email.com" class="form-control">
							<span class="input-group-btn">
							<button class="btn" type="submit">Submit</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 footer-block">
				<h2>Follow Us On</h2>
				<ul class="social-icons">
					<li>
						<a href="javascript:;" data-original-title="rss" class="rss"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="facebook" class="facebook"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="twitter" class="twitter"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="googleplus" class="googleplus"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="youtube" class="youtube"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="vimeo" class="vimeo"></a>
					</li>
				</ul>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 footer-block">
				<h2>Contacts</h2>
				<address class="margin-bottom-40">
				Phone: 800 123 3456<br>
				 Email: <a href="mailto:info@tweetafric.com">info@tweetafric.com</a>
				</address>
			</div>
		</div>
	</div>
</div>
<!-- END PRE-FOOTER -->

<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="container">
		 2015 ©Vibeink Ent. LTD. ALL Rights Reserved.
	</div>
</div>
<div class="scroll-to-top">
	<i class="icon-arrow-up"></i>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/respond.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script> <!-- extra-profile-->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/admin/pages/scripts/index3.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl;?>/admin/pages/scripts/profile.js" type="text/javascript"></script> <!-- extra-profile -->
<script src="<?php echo Yii::app()->baseUrl;?>/js/bootstrap3-typeahead.js" type="text/javascript"></script>
<?php
/*Yii::app()->clientScript->registerScriptFile(
        Yii::app()->request->baseUrl . '/js/bootstrap3-typeahead.js', CClientScript::POS_END);*/
?>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   Demo.init(); // init demo(theme settings page)
   Index.init(); // init index page
   Tasks.initDashboardWidget(); // init tash dashboard widget
   Profile.init(); // init page demo
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
