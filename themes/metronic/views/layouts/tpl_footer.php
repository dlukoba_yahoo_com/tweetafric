    <!-- BEGIN FOOTER -->
    <div class="footer">
      <div class="container">
        <div class="row">
          <!-- BEGIN COPYRIGHT -->
          <div class="col-md-7 col-sm-7 padding-top-10">
            2015 ©Vibeink Ent. LTD. ALL Rights Reserved. 
<a href="<?php echo Yii::app()->request->baseUrl.'/site/page?view=privacyPolicy';?>">Privacy Policy</a> | 
<a href="<?php echo Yii::app()->request->baseUrl.'/site/page?view=terms';?>">Terms of Service</a> | 
<a href="<?php echo Yii::app()->request->baseUrl.'/site/page?view=cookies';?>">Cookie Policy</a>
<a href="<?php echo Yii::app()->request->baseUrl.'/site/page?view=contacts';?>">Contact Us</a>
          </div>
          <!-- END COPYRIGHT -->
          <!-- BEGIN PAYMENTS -->
          <div class="col-md-5 col-sm-5">
            <ul class="social-footer list-unstyled list-inline pull-right">
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
<!--          <li><a href="#"><i class="fa fa-dribbble"></i></a></li> -->
              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-skype"></i></a></li>
<!--              <li><a href="#"><i class="fa fa-github"></i></a></li> -->
              <li><a href="#"><i class="fa fa-youtube"></i></a></li>
<!--              <li><a href="#"><i class="fa fa-dropbox"></i></a></li> -->
            </ul>  
          </div>
          <!-- END PAYMENTS -->
        </div>
      </div>
    </div>
    <!-- END FOOTER -->

<script src="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/jquery.min.js" type="text/javascript"></script>