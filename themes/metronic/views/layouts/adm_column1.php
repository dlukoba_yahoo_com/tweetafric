<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/adm_main'); ?>
<section class="main-body">
    <div class="container">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
	</div>
</section>
<?php $this->endContent(); ?>
