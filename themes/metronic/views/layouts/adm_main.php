<!-- Require the header -->
<?php require_once('adm_header.php')?>

<!-- Require the navigation -->
<?php require_once('adm_navigation.php')?>

<!-- Include content pages -->
<?php echo $content; ?>

<!-- Require the footer -->
<?php require_once('adm_footer.php')?>
