<?php $this->widget('bootstrap.widgets.TbNavbar', array(
    'brandLabel' => "<img src='". Yii::app()->theme->baseUrl  ."/frontend/layout/img/logos/tweetafric_logo.jpg'",
    'display' => TbHtml::NAVBAR_DISPLAY_FIXEDTOP, // default is static to top
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbNav',
            'items' => array(
                array('label' => 'Home', 'url' => Yii::app()->request->baseUrl, 'active' => true, 'items' => array(
                    array('label' => 'Tweetafric Home', 'url' => Yii::app()->request->baseUrl),
                    array('label' => 'Tweetafric Admin', 'url' => Yii::app()->request->baseUrl)
                )),
                array('label' => 'Free Followers', 'url' => Yii::app()->request->baseUrl.'/site/page?view=ffollowers'),
                array('label' => 'Are You A VIP?', 'url' => Yii::app()->request->baseUrl.'/site/page?view=ruvip'),
                array('label' => 'Browse', 'url' => Yii::app()->request->baseUrl.'/site/page?view=browse'),
                array('label' => 'Blog', 'url' => Yii::app()->request->baseUrl.'/site/page?view=blog'),
                array('label' => 'Talk To Us', 'url' => Yii::app()->request->baseUrl.'/site/page?view=contacts'),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbNav',
            'htmlOptions' => array(
                'class' => 'navbar-right'
            ),
            'items' => array(
                array('label' => (Yii::app()->user->id == NULL) ? 'Sign In' : 'Log Out', 
                    'url' => (Yii::app()->user->id == NULL) ? Yii::app()->request->baseUrl.'/twitter/connect' : Yii::app()->request->baseUrl.'/index.php/user/logout', 'active' => true)
                )
        )
    ),
)); ?>
