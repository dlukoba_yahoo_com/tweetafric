<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <title>Tweetafric Social Media</title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Tweetafric description" name="description">
  <meta content="Tweetafric twitter followers buy get keywords" name="keywords">
  <meta content="kaylayshya" name="author">

  <meta property="og:site_name" content="-CUSTOMER VALUE-">
  <meta property="og:title" content="-CUSTOMER VALUE-">
  <meta property="og:description" content="-CUSTOMER VALUE-">
  <meta property="og:type" content="website">
  <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
  <meta property="og:url" content="-CUSTOMER VALUE-">

  <link rel="shortcut icon" href="favicon.ico">

  <!-- Fonts START -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <!-- Fonts END -->

  <!-- Global styles START -->          
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <?php Yii::app()->bootstrap->register(); ?>
  <!-- Global styles END --> 
   
  <!-- Page level plugin styles START -->
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css" rel="stylesheet">
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/global/css/components.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/frontend/layout/css/style.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/frontend/pages/css/style-revolution-slider.css" rel="stylesheet"><!-- metronic revo slider styles -->
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/frontend/layout/css/style-responsive.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/frontend/layout/css/themes/orange.css" rel="stylesheet" id="style-color">
  <link href="<?php echo Yii::app()->theme->baseUrl;?>/frontend/layout/css/custom.css" rel="stylesheet">
  <!-- Theme styles END -->
<!-- This section contains fonts as discussed with tweetafric guys -->

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Amaranth" />
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Ubuntu" />
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans">
<style type="text/css">
   .allAmaranth { 
    font-family: Amaranth;
    font-style: normal;
    font-variant: normal;
    font-weight: 500;
    }
.allUbuntu { 
	font-family: Ubuntu; 
}
.tbody{
        font-family: Ubuntu;
}
@font-face {
    font-family: 'BebasNeueRegular';
    src:url(/var/www/html/csites/tweetafric/app/themes/metronic/frontend/fonts/BebasNeue Regular.ttf') format('truetype');
        
    font-weight: normal;
    font-style: normal;
}
@font-face {
	font-family:"AddamsRegular";
	src:url("/var/www/html/csites/tweetafric/app/themes/metronic/frontend/fonts/AddamsRegular/AddamsRegular.eot?") format("eot"),
	url("/var/www/html/csites/tweetafric/app/themes/metronic/frontend/fonts/AddamsRegular/AddamsRegular.woff") format("woff"),
	url("/var/www/html/csites/tweetafric/app/themes/metronic/frontend/fonts/AddamsRegular/AddamsRegular.ttf") format("truetype"),
	url("/var/www/html/csites/tweetafric/app/themes/metronic/frontend/fonts/AddamsRegular/AddamsRegular.svg#AddamsRegular") format("svg");
	font-weight:normal;
	font-style:normal;
	}
.allbabesneue{
	font-family: "BebasNeueRegular";
}
.allAddams{
	font-family: "AddamsRegular";
}
@font-face {
    font-family: 'Sansation_Regular';
    src:url(/var/www/html/csites/tweetafric/app/themes/metronic/frontend/fonts/sansation/Sansation_Regular.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
}
.allSansation{
        font-family: "Sansation_Regular";
}

body{
        font-family: "Open Sans" !important;
        text-align:justify;

}

body{
        font-family: "Open Sans" !important;
        text-align:justify;

}

 </style>
<!-- end of tweetafric customizations -->
</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate" style="text-align:justify; font-family:'Open Sans';">
<?php
/*
    <!-- BEGIN STYLE CUSTOMIZER -->
    <div class="color-panel hidden-sm">
      <div class="color-mode-icons icon-color"></div>
      <div class="color-mode-icons icon-color-close"></div>
      <div class="color-mode">
        <p>THEME COLOR</p>
        <ul class="inline">
          <li class="color-red" data-style="red"></li>
          <li class="color-blue current color-default" data-style="blue"></li>
          <li class="color-green" data-style="green"></li>
          <li class="color-orange" data-style="orange"></li>
          <li class="color-gray" data-style="gray"></li>
          <li class="color-turquoise" data-style="turquoise"></li>
        </ul>
      </div>
    </div>
    <!-- END BEGIN STYLE CUSTOMIZER --> 
*/
?>
    <!-- BEGIN TOP BAR -->
<!--    <div class="pre-header">
        <div class="container">
            <div class="row">
                 BEGIN TOP BAR LEFT PART 
                <div class="col-md-6 col-sm-6 additional-shop-info">
                    <ul class="list-unstyled list-inline">
                        <li><i class="fa fa-phone"></i><span>+254 722 111222</span></li>
                        <li><i class="fa fa-envelope-o"></i><span>info@tweetafric.com</span></li>
                    </ul>
                </div>
                 END TOP BAR LEFT PART 
                 BEGIN TOP BAR MENU 
                <div class="col-md-6 col-sm-6 additional-nav">
                    <ul class="list-unstyled list-inline pull-right">
                        <li><a href="<?php echo Yii::app()->request->baseUrl.'/index.php/site/login';?>">Log In</a></li>
                        <li><a href="<?php echo Yii::app()->request->baseUrl.'/index.php/site/login';?>">Registration</a></li>
                    </ul>
                </div>
                 END TOP BAR MENU 
            </div>
        </div>        
    </div>-->
    <!-- END TOP BAR -->
