<?php
/**
* 
*/
class TwitterHelper
{
    public static function HumanReadableTime($time)
	{
		$now = strtotime('now');
		$created = strtotime($time);
		$difference = $now - $created;

		$minute = 60;
	    $hour = $minute * 60;
	    $day = $hour * 24;
	    $week = $day * 7;

	    if ( is_numeric($difference) && $difference > 0) {
	    	// If less than 3 seconds.
	    	if ($difference < 3) {
	            return 'right now';
	        }
	        // If less than minute.
	        if ($difference < $minute) {
	            return floor($difference) . ' seconds ago';
	        }
	        // If less than 2 minutes.
	        if ($difference < $minute * 2) {
	            return 'about 1 minute ago';
	        }
	        // If less than hour.
	        if ($difference < $hour) {
	            return floor( $difference / $minute ) . ' minutes ago';
	        }
	        // If less than 2 hours.
	        if ($difference < $hour * 2) {
	            return 'about 1 hour ago';
	        }
	        // If less than day.
	        if ( $difference < $day ) {
	            return floor($difference / $hour) . ' hours ago';
	        }
	        // If more than day, but less than 2 days.
	        if ($difference > $day && $difference < $day * 2) {
	            return 'yesterday';
	        }
	        // If less than year.
	        if ($difference < $day * 365) {
	            return floor($difference / $day) . ' days ago';
	        }
	        // Else return more than a year.
	        return 'over a year ago';
	    }
	}
        
        public static function SanitizeLinks($tweet) {
            if(isset($tweet->retweeted_status)) {
                $rt_section = current(explode(":", $tweet->text));
                $text = $rt_section.": ";
                $text .= $tweet->retweeted_status->text;
            } else {
                $text = $tweet->text;
            }
            $text = preg_replace('/((http)+(s)?:\/\/[^<>\s]+)/i', '<a href="$0" target="_blank" rel="nofollow">$0</a>', $text );
            $text = preg_replace('/[@]+([A-Za-z0-9-_]+)/', '<a href="http://twitter.com/$1" target="_blank" rel="nofollow">@$1</a>', $text );
            $text = preg_replace('/[#]+([A-Za-z0-9-_]+)/', '<a href="http://twitter.com/search?q=%23$1" target="_blank" rel="nofollow">$0</a>', $text );
            return $text;

	}
}
?>
