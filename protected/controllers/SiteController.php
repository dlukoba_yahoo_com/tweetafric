<?php

class SiteController extends Controller
{
	//public $layout='//layouts/column1';
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */

    public function actionIndex()
    {
        //take non logged in users to the default home page
        if(Yii::app()->user->isGuest and @Yii::app()->session['status'] != 'verified') {
            $this->render('index',array());
            $this->layout='home';
        }else{ 
            $criteria = new CDbCriteria;
            $criteria->select = array('screen_name', 'location', 'url', 'description', 'followers_count', 
                'friends_count', 'statuses_count', 'time_zone', 'countryid');
            $criteria->condition = 'twitter_user_id=:twitter_user_id';
            $criteria->params = array(':twitter_user_id' => Yii::app()->session['access_token']['user_id']);
            $userdata = TwitterUser::model()
                    ->find($criteria);
            
            ////take logged in users to the dashboard
            //$this->layout='//layouts/adm_main'; //works
            $this->layout='//layouts/adm_column1';	//works too
            $this->render('dashboard',array('userdata' => $userdata));
            //$this->render('landing',array());
        }
    }
        
public function actionDash()
	{

			$this->layout='//layouts/adm_main';
			$this->render('dashboard',array());
	}
	public function actionIndexx()
	{
	  if(!Yii::app()->user->isGuest) {
//  	  if (UserSetting::model()->count()==0 or !UserSetting::model()->checkConfiguration(Yii::app()->user->id))
//        $this->redirect(array('/usersetting/update'));
  	  //else 
  	  if (Account::model()->count()==0)
        $this->redirect(array('/account/create'));
      else
        $this->redirect(array('/tweet/index'));	    
	  }
	  	  
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'

		$this->layout='home';
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		$this->render('index',array('model'=>$model));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', array('error' => $error));
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

	      $mailgun = new Mailgun();
  // to do - sending headers not from contact form
    $mailgun->mail("$name <{$model->email}>",Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		$this->layout='//layouts/login_soft';//login soft
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		// collect user input data  was LoginForm before
		if(isset($_POST['LoginForm']))
		{
			//print_r($_POST); die();
			$model->attributes= $_POST; //was $_POST['LoginForm']
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				$loginStatus = 1;
				$this->redirect(Yii::app()->user->returnUrl);
			}else{
				Yii::app()->user->setFlash('loginMessage','Wrong Username / Password');	
			}
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        Yii::app()->session->clear();
        Yii::app()->session->destroy();

        $this->redirect(Yii::app()->homeUrl);
    }
}
