<?php
class PaymentController extends Controller
{
	/**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/main';
    /**
     * @return array action filters
     */
    public function filters()
    {
            return array(
                    'accessControl', // perform access control for CRUD operations
            );
    }
        
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                    'actions'=>array('index'),
                    'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                    'actions'=>array('checkout', 'confirm'),
                    'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                    'actions'=>array('delete'),
                    'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                    'users'=>array('*'),
            ),
        );
    }
        
    public function actionCheckout($id) {
        if(isset($_POST) && !empty($_POST))
        {
            $paymentTerm = Paymentterm::model()->
                find('id=:id', array(':id' => $id));
            
            $items = array();
            $items[] = array('name' => $paymentTerm->name, 'amt' => $paymentTerm->amount, 'qty' => 1);
            $paymentAmount = $paymentTerm->amount;
            $currencyCodeType = "USD";
            $paymentType = "SALE";
            
            $resArray = Yii::app()->paypal->SetExpressCheckoutDG( $paymentAmount, $currencyCodeType, $paymentType, $items );
            $ack = strtoupper($resArray["ACK"]);
            if($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING")
            {
                $token = urldecode($resArray["TOKEN"]);
                Yii::app()->paypal->RedirectToPayPalDG( $token );
            }
        }
        $paymentTerm = Paymentterm::model()->
                find('id=:id', array(':id' => $id));

        $this->layout='//layouts/adm_column1';  //works too
        $this->render('checkout', array('paymentType' => $id,
            'paymentTerm' => $paymentTerm));
    }

    public function actionConfirm()
    {
        $token = Yii::app()->request->getParam('token');
        $res = Yii::app()->paypal->GetExpressCheckoutDetails($token);

        $finalPaymentAmount =  $res["PAYMENTREQUEST_0_AMT"];
        $payerID = Yii::app()->request->getParam('PayerID');
        $paymentType = 'Sale';
        $currencyCodeType = $res['CURRENCYCODE'];
        $items = array();
        
        $i = 0;
        // adding item details those set in setExpressCheckout
        while(isset($res["L_PAYMENTREQUEST_0_NAME$i"]))
        {
            $items[] = array('name' => $res["L_PAYMENTREQUEST_0_NAME$i"], 'amt' => $res["L_PAYMENTREQUEST_0_AMT$i"], 'qty' => $res["L_PAYMENTREQUEST_0_QTY$i"]);
            $i++;
        }

        $resArray = Yii::app()->paypal->ConfirmPayment( $token, $paymentType, $currencyCodeType, $payerID, $finalPaymentAmount, $items );
        $ack = strtoupper($resArray["ACK"]);
        if( $ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING" )
        {
            $transactionId      = $resArray["PAYMENTINFO_0_TRANSACTIONID"]; // Unique transaction ID of the payment.
            $transactionType    = $resArray["PAYMENTINFO_0_TRANSACTIONTYPE"]; // The type of transaction Possible values: l  cart l  express-checkout
            $paymentType        = $resArray["PAYMENTINFO_0_PAYMENTTYPE"];  // Indicates whether the payment is instant or delayed. Possible values: l  none l  echeck l  instant
            $orderTime          = $resArray["PAYMENTINFO_0_ORDERTIME"];  // Time/date stamp of payment
            $amt                = $resArray["PAYMENTINFO_0_AMT"];  // The final amount charged, including any  taxes from your Merchant Profile.
            $currencyCode       = $resArray["PAYMENTINFO_0_CURRENCYCODE"];  // A three-character currency code for one of the currencies listed in PayPay-Supported Transactional Currencies. Default: USD.
            $feeAmt             = $resArray["PAYMENTINFO_0_FEEAMT"];  // PayPal fee amount charged for the transaction
        //  $settleAmt          = $resArray["PAYMENTINFO_0_SETTLEAMT"];  // Amount deposited in your PayPal account after a currency conversion.
            $taxAmt             = $resArray["PAYMENTINFO_0_TAXAMT"];  // Tax charged on the transaction.
        //  $exchangeRate       = $resArray["PAYMENTINFO_0_EXCHANGERATE"];  // Exchange rate if a currency conversion occurred. Relevant only if your are billing in their non-primary currency. If the customer chooses to pay with a currency other than the non-primary currency, the conversion occurs in the customer's account.

            /*
            ' Status of the payment:
            'Completed: The payment has been completed, and the funds have been added successfully to your account balance.
            'Pending: The payment is pending. See the PendingReason element for more information.
            */
            $paymentStatus = $resArray["PAYMENTINFO_0_PAYMENTSTATUS"];

            /*
             'The reason the payment is pending:
             '  none: No pending reason
             '  address: The payment is pending because your customer did not include a confirmed shipping address and your Payment Receiving Preferences is set such that you want to manually accept or deny each of these payments. To change your preference, go to the Preferences section of your Profile.
             '  echeck: The payment is pending because it was made by an eCheck that has not yet cleared.
             '  intl: The payment is pending because you hold a non-U.S. account and do not have a withdrawal mechanism. You must manually accept or deny this payment from your Account Overview.
             '  multi-currency: You do not have a balance in the currency sent, and you do not have your Payment Receiving Preferences set to automatically convert and accept this payment. You must manually accept or deny this payment.
             '  verify: The payment is pending because you are not yet verified. You must verify your account before you can accept this payment.
             '  other: The payment is pending for a reason other than those listed above. For more information, contact PayPal customer service.
             */
             $pendingReason = $resArray["PAYMENTINFO_0_PENDINGREASON"];
             
             //save in db
             $paypalResponse = new Paypalresponse;
             $paypalResponse->transactionid = $transactionId;
             $paypalResponse->transactiontype = $transactionType;
             $paypalResponse->paymenttype = $paymentType;
             $paypalResponse->ordertime = $orderTime;
             $paypalResponse->transactionamount = $amt;
             $paypalResponse->currencycode = $currencyCode;
             $paypalResponse->feeamount = $feeAmt;
             $paypalResponse->taxamount = $taxAmt;
             $paypalResponse->paymentstatus = $paymentStatus;
             $paypalResponse->pendingreason = $pendingReason;
             
             $user = TwitterUser::model()
                    ->find('twitter_user_id=:twitter_user_id', 
                            array(':twitter_user_id' => Yii::app()->session['access_token']['user_id']));
             $paymentTerm = Paymentterm::model()
                    ->find('amount=:amount', 
                            array(':amount' => $amt));
             $paypalResponse->userid = $user->id;
             $paypalResponse->paymenttermref = $paymentTerm->id;
             $paypalResponse->insert();
        }

        $this->renderPartial('_confirm', array('resArray' => $resArray), FALSE, TRUE);
    }
}