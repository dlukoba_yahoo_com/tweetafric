<?php
//http://code.tutsplus.com/tutorials/building-with-the-twitter-api-oauth-reading-and-posting--cms-22193
//http://www.yiiframework.com/extension/twitter/
class TwitterController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/main';
    /**
     * @return array action filters
     */
    public function filters()
    {
            return array(
                    'accessControl', // perform access control for CRUD operations
            );
    }
        
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                    'actions'=>array('index','callback','connect', 'celeb'),
                    'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                    'actions'=>array('update', 'myWall', 'followuser', 'unfollowuser', 'fetchLatestItems', 
                        'configureCountry', 'updateUserCountry', 'fetchTrends', 'updateCountryConfig', 
                        'tweet', 'retweet', 'favorite', 'addCeleb', 'changeUsertype', 'invite', 'search'),
                    'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                    'actions'=>array('delete'),
                    'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                    'users'=>array('*'),
            ),
        );
    }
        
    public function actionIndex() {
        echo 'index';
    }

    public function actionConnect()
    {
        /*
		to be extended to have attach account for logged in user.
		unset(Yii::app()->session['account_id']);
        Yii::app()->session['account_id']=$_GET['id'];
		*/
        $twitter = Yii::app()->twitter->getTwitter();
        $request_token = $twitter->getRequestToken();
        
        //set some session info
        Yii::app()->session['oauth_token'] = $token =$request_token['oauth_token'];
        Yii::app()->session['oauth_token_secret'] = $request_token['oauth_token_secret'];
       
        if ($twitter->http_code == 200) {
            //get twitter connect url
            $url = $twitter->getAuthorizeURL($token);
            //send them              
            Yii::app()->request->redirect($url);
        }else{
           //error here
           $this->redirect(Yii::app()->homeUrl);
        }
    }
    
    public function actionCallback() {
        /* If the oauth_token is old redirect to the connect page. */
        if (isset($_REQUEST['oauth_token']) && Yii::app()->session['oauth_token'] !== $_REQUEST['oauth_token']) {
            Yii::app()->session['oauth_status'] = 'oldtoken';
        }
        /* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
        $twitter = Yii::app()->twitter->
                getTwitterTokened(Yii::app()->session['oauth_token'], Yii::app()->session['oauth_token_secret']);   
        
        /* Request access tokens from twitter */
        $access_token = $twitter->getAccessToken($_REQUEST['oauth_verifier']);
        
        /* Save the access tokens. Normally these would be saved in a database for future use. */            
        Yii::app()->session['access_token'] = $access_token;
        /*            $account = Account::model()->findByAttributes(array('user_id'=>Yii::app()->user->id,'id'=>Yii::app()->session['account_id']));
            $account['oauth_token'] = $access_token['oauth_token'];
            $account['oauth_token_secret'] = $access_token['oauth_token_secret'];
        $account->save();
         */           
        /* Remove no longer needed request tokens */
        unset(Yii::app()->session['oauth_token']);
        unset(Yii::app()->session['oauth_token_secret']);
        
        if (200 == $twitter->http_code) {
            /*				$model=new UserLogin;
			$attrs = array('username'=>'admin','password'=>'!23qwe');
			$model->attributes=$attrs;
			
			if($model->validate()) {
			}else{
				echo 'paaaaaaaaaaaaaa';die();
			}
*/
            /* The user has been verified and the access tokens can be saved for future use */
            Yii::app()->session['status'] = 'verified';
            $twitter = Yii::app()->twitter->getTwitterTokened($access_token['oauth_token'], $access_token['oauth_token_secret']);
            
            //get user details, ignore the statuses here
            $twuser= $twitter->get("account/verify_credentials", array('skip_status' => TRUE));
                        
            $user = TwitterUser::model()
                    ->find('twitter_user_id=:twitter_user_id', array(':twitter_user_id' => $twuser->id));
            //user doesn't exist, create a new one
            if($user == NULL)
            {
                /*$newUser = new TwitterUser;
                $newUser->twitter_user_id = $twuser->id;
                $newUser->screen_name = $twuser->screen_name;
                $newUser->name = $twuser->name;
                $newUser->profile_image_url = $twuser->profile_image_url;
                $newUser->location = $twuser->location;
                $newUser->url = $twuser->url;
                $newUser->description = $twuser->description;
                $newUser->followers_count = $twuser->followers_count;
                $newUser->friends_count = $twuser->friends_count;
                $newUser->statuses_count = $twuser->statuses_count;
                $newUser->usertype = TwitterUser::USERTYPE_NORMAL;
                $newUser->time_zone = $twuser->time_zone;
                $newUser->created_at = date('Y-m-d H:i:s', strtotime($twuser->created_at));
                $newUser->modified_at = date('Y-m-d H:i:s');
                $newUser->date_entry_recorded = date('Y-m-d H:i:s');*/
                $user_obj = array('id' => $twuser->id, 'screen_name' => $twuser->screen_name, 'name' => $twuser->name,
                    'profile_image_url' => $twuser->profile_image_url, 'location' => $twuser->location,
                    'url' => $twuser->url, 'description' => $twuser->description, 
                    'followers_count' => $twuser->followers_count, 'friends_count' => $twuser->friends_count,
                    'statuses_count' => $twuser->statuses_count, 'time_zone' => $twuser->time_zone,
                    'created_at' => $twuser->created_at, 
                    'oauth_token' => Yii::app()->session['access_token']['oauth_token'], 
                    'oauth_token' => Yii::app()->session['access_token']['oauth_token_secret']);
                $newUser = TwitterUser::model()->addNormalUser($user_obj);
                $newUser->save();
            }
            //update the record
            else {
                $user->name = $twuser->name;
                $user->location = $twuser->location;
                $user->followers_count = $twuser->followers_count;
                $user->friends_count = $twuser->friends_count;
                $user->time_zone = $twuser->time_zone;
                $user->profile_image_url = $twuser->profile_image_url;
                $user->oauth_token = Yii::app()->session['access_token']['oauth_token'];
                $user->oauth_token_secret = Yii::app()->session['access_token']['oauth_token_secret'];
                $user->save();
            }
            $identity=new UserIdentity($twuser->id, '!23qwe');
            
            //for now use twitter_user_id
            $identity->hybridauth($twuser->id);
            if ($identity->errorCode != UserIdentity::ERROR_NONE){
                    echo "ERROR - - ".$identity->errorCode; 
                    die();
            }
            Yii::app()->user->login($identity);
            //$identity->setState('id', $identity->id);
            //$identity->setState('name', $identity->username);
                        
            Yii::app()->session['twitter_name'] = $twuser->name;
            Yii::app()->session['profile_image_url'] = $twuser->profile_image_url;
            			
            $this->redirect(array('site/index'));

        } else {
            /* Save HTTP status for error dialog on connnect page.*/
            //header('Location: /clearsessions.php');
            $this->redirect(Yii::app()->homeUrl);
        } 
    }
    
    public function actionMyWall()
    {
        $accessToken = Yii::app()->session['access_token'];
        $twitter = Yii::app()->twitter->getTwitterTokened($accessToken['oauth_token'], 
                $accessToken['oauth_token_secret']);
        
        $userdata = TwitterUser::model()
                ->with(array('categories'))
                ->find(array(
                    'condition' => 'twitter_user_id=:twitter_user_id',
                    'select' => array('screen_name', 'location', 'url', 'description', 'followers_count', 
                        'friends_count', 'statuses_count', 'time_zone', 'countryid'),
                    'params' => array(':twitter_user_id' => Yii::app()->session['access_token']['user_id'])
                ));
        $user_cats = [];
        foreach($userdata->categories as $category)
        {
            $user_cats[$category['id']] = array('name' => $category['name'], 'isVisible' => FALSE);
        }
        
        $command = Yii::app()->db->createCommand("call `get_most_followed_users_proc`(". $userdata['countryid']. ")");
        $most_followed_users_in_categories = $command->queryAll();
        
        //$distinct_categories = [];
        $user_categories = [];
        
        foreach ($most_followed_users_in_categories as $most_followed_users_in_category) {
            if(array_key_exists($most_followed_users_in_category['category_id'], $user_cats) && 
                    ($most_followed_users_in_category['countryid'] == $userdata['countryid'] ||
                    $most_followed_users_in_category['countryid'] == 53/*Global*/))
            {
                $user_categories[] = $most_followed_users_in_category;
                $user_cats[$most_followed_users_in_category['category_id']]['isVisible'] = TRUE;
                //$distinct_categories[$most_followed_users_in_category['category_id']] = $most_followed_users_in_category['category_name'];
            }
        }
        
        //remove undisplayable items
        foreach($user_cats as $key => $user_cat)
        {
            if(!$user_cat['isVisible'])
                unset ($user_cats[$key]);
        }
        
        $this->layout='//layouts/adm_column1';	//works too
        $this->render('myWall', array(
                'categories' => $user_cats/*$distinct_categories*/,
                'most_followed_users_in_categories' => $user_categories/*$most_followed_users_in_categories*/,
                'userdata' => $userdata
            )
        );
    }
    
    public function actionFollowuser()
    {
        $accessToken = Yii::app()->session['access_token'];
        $userToFollowId = Yii::app()->request->getPost('user_to_follow');
        
        $twitter = Yii::app()->twitter->getTwitterTokened($accessToken['oauth_token'], 
                $accessToken['oauth_token_secret']);
        $result = $twitter->post('friendships/create', array('user_id' => (int)$userToFollowId));
        $hasPostFailed = array_key_exists('errors', $result);
        
        if($hasPostFailed)
            echo 'The action failed!';
        else
            echo 'The user was followed successfully';
        
        Yii::app()->end();
    }
    
    public function actionUnfollowuser()
    {
        $accessToken = Yii::app()->session['access_token'];
        $userToUnFollowId = Yii::app()->request->getPost('user_to_unfollow');
        
        $twitter = Yii::app()->twitter->getTwitterTokened($accessToken['oauth_token'], 
                $accessToken['oauth_token_secret']);
        $twitter->post('friendships/destroy', array('user_id' => (int)$userToUnFollowId));
        
        echo "Request sent successfully";
    }
    
    public function actionFetchLatestItems()
    {
        $accessToken = Yii::app()->session['access_token'];
        $twitter = Yii::app()->twitter->getTwitterTokened($accessToken['oauth_token'], 
                $accessToken['oauth_token_secret']);
        $api = Yii::app()->request->getPost('api');
        $result = [];
        
        switch ($api)
        {
            //get 10 latest tweets by user+friends
            case "timeline":
                $statuses = (array)$twitter->get('statuses/home_timeline', 
                        array('count' => 10));
                
                foreach($statuses as $status)
                {
                    $row = [];
                    $row['created_at'] = $status->created_at;
                    $row['text'] = TwitterHelper::SanitizeLinks($status);
                    $row['name'] = $status->user->name;
                    $row['screen_name'] = $status->user->screen_name;
                    $row['tweet_id'] = $status->id_str;
                    $row['profile_image'] = $status->user->profile_image_url;
                    $row['favourite_count'] = $status->favorite_count;
                    $row['retweet_count'] = $status->retweet_count;
                    $result[] = $row;
                }
                $this->renderPartial('_timelineRow', array('statuses' => $result), false, false);
                
                break;
            case "mentions":
                $mentions = (array)$twitter->get('statuses/mentions_timeline', 
                        array('count' => 10, 'include_entities' => FALSE));
                foreach($mentions as $mention)
                {
                    $row = [];
                    $row['created_at'] = $mention->created_at;
                    $row['text'] = TwitterHelper::SanitizeLinks($mention);
                    $row['name'] = $mention->user->name;
                    $row['screen_name'] = $mention->user->screen_name;
                    $row['tweet_id'] = $mention->id_str;
                    $row['profile_image'] = $mention->user->profile_image_url;
                    $row['favourite_count'] = $mention->favorite_count;
                    $row['retweet_count'] = $mention->retweet_count;
                    $result[] = $row;
                }
                $this->renderPartial('_mentionRow', array('mentions' => $result), false, false);
                
                break;
        }
    }
	
	public function actionConfigureCountry()
	{
            $model = new \UserConfigForm;
            $countries = Country::model()
                    ->findAll(array(
                        'order' => 'country_name asc',
                        'select' => array('id', 'country_name')
                    ));
            $categories = Category::model()->
                    findAll(array(
                        'order' => 'name asc', 
                        'select' => array('id', 'name')
                    )
            );
            $user = TwitterUser::model()
                    ->find('twitter_user_id=:twitter_user_id', 
                            array(':twitter_user_id' => Yii::app()->session['access_token']['user_id']));
            $cats = array();
            foreach ($user->categories as $category)
            {
                $cats[$category['id']] = array('selected' => TRUE);
            }
            
            $isInSaveMode = Yii::app()->request->getPost('UserConfigForm');
            if(isset($isInSaveMode))
            {
                $model->attributes=$_POST['UserConfigForm'];
                if($model->validate())
                {
                    $user->countryid = $model->country_id;
                    $user->categories = $model->category_ids;
                    
                    $user->save();
                }
                else
                {
                    $this->renderPartial('configureCountry', 
                            array(
                                'model' => $model, 
                                'countries' => $countries, 
                                'categories' => $categories, 
                                'country_id' => $user->countryid,
                                'currentCategories' => $cats), 
                            FALSE, 
                            TRUE);
                    Yii::app()->end();
                }
                
                echo 'Your profile has been updated successfully';
                Yii::app()->end();
            }
            
            $this->renderPartial('configureCountry', 
                    array(
                        'model' => $model, 
                        'countries' => $countries, 
                        'categories' => $categories, 
                        'country_id' => $user->countryid,
                        'currentCategories' => $cats), 
                    FALSE, 
                    TRUE);
	}

    public function actionFetchTrends()
    {
        $accessToken = Yii::app()->session['access_token'];
        $countryWoeid = Yii::app()->db->createCommand()->select('woeid')
                ->from('bc_twitter_user tu')
                ->join('bc_countries c', 'tu.countryid=c.id')
                ->where('tu.twitter_user_id=:twitter_user_id', 
                        array(':twitter_user_id'=> $accessToken['user_id']))
                ->queryScalar();
        if(!$countryWoeid)
        {
            echo '<div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                    Please configure your country in order to use this feature
            </div>';
            Yii::app()->end();
        }
        
        $twitter = Yii::app()->twitter->getTwitterTokened($accessToken['oauth_token'], 
                $accessToken['oauth_token_secret']);

        $result = (array)$twitter->get('trends/place', 
                        array('id' => $countryWoeid));
        $this->renderPartial('_trendsRow', 
                array('trends' => $result[0]->trends, 'as_of' => TwitterHelper::HumanReadableTime($result[0]->as_of)), false, false);
    }
    
    public function actionInvite()
    {
        $email = Yii::app()->request->getPost('email');
        if(isset($email))
        {
            $v = new CEmailValidator();
            $res = $v->validateValue($email);
            
            if($res == TRUE)
            {
                $accessToken = Yii::app()->session['access_token'];
                $url = Yii::app()->getBaseUrl(true);
                $params = array('url' => $url, 'name' => $accessToken['screen_name']);
                /*$message = new YiiMailMessage;
                $message->view = "_mailInvite";
                $message->subject = "Tweetafric Invitation";
                
                
                $message->setBody($params, 'text/html');
                $message->addTo($email);
                $message->from = "daniel@lukoba.com";
                //ob_start();
                Yii::app()->mail->send($message);*/
                $mail = new YiiMailer('_mailInvite', $params);
                $mail->clearLayout();
                $mail->setFrom('daniel@lukoba.com', 'Tweetafric');
                $mail->setTo($email);
                $mail->setSubject('Tweetafric Invitation');
                
                if($mail->send())
                    echo ' A link has been sent to ' . $email;
                else {
                    echo 'error '. $mail->getError();
                }    
                
                Yii::app()->end();
            }
        }
        $this->renderPartial('_invite', array(), FALSE, TRUE);
    }
    
    public function actionTweet()
    {
        //get values for retweeting
        $tweet_id = Yii::app()->request->getParam('tweet_id');
        $username = Yii::app()->request->getParam('username');
        
        $tweet = Yii::app()->request->getPost('tweet');
        if(isset($tweet) && strlen($tweet) > 0)
        {
            $in_reply_to_status_id = Yii::app()->request->getPost('tweet_id');
            $accessToken = Yii::app()->session['access_token'];
            $twitter = Yii::app()->twitter->getTwitterTokened($accessToken['oauth_token'], 
                    $accessToken['oauth_token_secret']);
            $tweet = (strlen($tweet) > 140) ? substr($tweet, 0, 140) : $tweet;
            $result = (array)$twitter->post('statuses/update', 
                    array('status' => $tweet, 'display_coordinates' => FALSE, 'trim_user' => TRUE,
                        'in_reply_to_status_id' => $in_reply_to_status_id));
            $hasPostFailed = array_key_exists('errors', $result);
            //is successful
            if (!$hasPostFailed && !empty($result)) {
                //save to db
                $tweet = new Tweet;
                $tweet->created_at = date('Y-m-d H:i:s');
                $tweet->is_rt = isset($in_reply_to_status_id) ? TRUE : FALSE;
                $tweet->twitter_user_id = $accessToken['user_id'];
                $tweet->tweet_id = $result['id'];
                $tweet->tweet_text = $result['text'];
                $tweet->save();

                echo 'Tweet posted successfully';
                Yii::app()->end();
            }
            else
            {
                echo 'Error posting your tweet!';
                Yii::app()->end();
            }
        }
        $this->renderPartial('_tweet', array('tweet_id' => $tweet_id, 'username' => $username), FALSE, TRUE);
    }
    
    public function actionRetweet()
    {
        $tweet_id = Yii::app()->request->getPost('tweet_id');
        $accessToken = Yii::app()->session['access_token'];
        $twitter = Yii::app()->twitter->getTwitterTokened($accessToken['oauth_token'], 
                $accessToken['oauth_token_secret']);
        $result = (array)$twitter->post('statuses/retweet/'. $tweet_id, 
                    array('trim_user' => TRUE));
        $hasPostFailed = array_key_exists('errors', $result);
        
        if($hasPostFailed)
            echo 'Posting the retweet failed!';
        else
            echo 'The retweet was done successfully';
        
        Yii::app()->end();
    }
    
    public function actionFavorite()
    {
        $tweet_id = Yii::app()->request->getPost('tweet_id');
        $accessToken = Yii::app()->session['access_token'];
        $twitter = Yii::app()->twitter->getTwitterTokened($accessToken['oauth_token'], 
                $accessToken['oauth_token_secret']);
        $result = (array)$twitter->post('favorites/create', 
                    array('id' => $tweet_id));
        $hasPostFailed = array_key_exists('errors', $result);
        
        if($hasPostFailed)
            echo 'Posting the favorite failed!';
        else
            echo 'The tweet was favorited successfully';
        
        Yii::app()->end();
    }
    
    public function actionAddCeleb()
    {
        $model = new CelebForm;
        $countries = Country::model()->findAll(array(
            'order' => 'country_name asc',
            'select' => array('id', 'country_name')
        ));

        $categories = Category::model()->findAll(array(
            'order' => 'name asc', 
            'select' => array('id', 'name')
            )
        );
        
        $isSaveMode = Yii::app()->request->getPost('CelebForm');
        if(isset($isSaveMode))
        {
            $model->attributes=$_POST['CelebForm'];

            //save the data if valid
            if($model->validate())
            {
                //confirm that user does not already exist in the DB
                $twitter_username = $model->twitter_username;
                $userAlreadyExists = TwitterUser::model()->exists('screen_name=:screen_name', array(':screen_name' => $twitter_username));
                if($userAlreadyExists)
                {
                    $this->layout = '//layouts/adm_column1';
                    $this->render('addCeleb', array('model' => $model, 'countries' => $countries, 'categories' => $categories));
                }
            
                $country_id = $model->country_id;
            
                //fetch data for user from twitter
                $accessToken = Yii::app()->session['access_token'];
                $twitter = Yii::app()->twitter->getTwitterTokened($accessToken['oauth_token'], 
                    $accessToken['oauth_token_secret']);
                $result = (array)$twitter->get('users/show', 
                    array('screen_name' => $twitter_username, 'include_entities' => FALSE));
                $hasPostFailed = array_key_exists('errors', $result);
            
                //the user was not found on twitter
                if($hasPostFailed)
                {
                    $this->layout = '//layouts/adm_column1';
                    $this->render('addCeleb', array('model' => $model, 'countries' => $countries, 'categories' => $categories));
                    Yii::app()->end();
                }
                else
                {
                    //create user with data
                    $user_obj = array('id' => $result['id'], 'screen_name' => $result['screen_name'], 'name' => $result['name'],
                        'profile_image_url' => $result['profile_image_url'], 'location' => $result['location'],
                        'url' => $result['url'], 'description' => $result['description'], 
                        'followers_count' => $result['followers_count'], 'friends_count' => $result['friends_count'],
                        'statuses_count' => $result['statuses_count'], 'time_zone' => $result['time_zone'],
                        'created_at' => $result['created_at'], 'country_id' => $country_id);
                    $user = TwitterUser::model()->addCelebUser($user_obj);
                    $user->categories = $model->category_ids;
                    $user->save();
                    
                    $this->redirect(array('twitter/celeb', 'id' => $result['screen_name']));
                }
            }
            
            $this->layout = '//layouts/adm_column1';
            $this->render('addCeleb', array('model' => $model, 'countries' => $countries, 'categories' => $categories));            
            
            Yii::app()->end();
        }
        
        $this->layout = '//layouts/adm_column1';
        $this->render('addCeleb', array('model' => $model, 'countries' => $countries, 'categories' => $categories));
    }
    
    public function actionCeleb($id)
    {
        $celeb = TwitterUser::model()->find('screen_name=:screen_name', array(':screen_name' => $id));
        if($celeb === null)
            throw new CHttpException(404, 'The celeb requested does not exist.');
        
        $accessToken = Yii::app()->session['access_token'];
        $twitter = Yii::app()->twitter->getTwitterTokened($accessToken['oauth_token'], 
                $accessToken['oauth_token_secret']);
        $statuses = (array)$twitter->get('statuses/user_timeline', array('count' => 5, 'trim_user' => TRUE, 
            'screen_name' => $id));
        $result = [];
        
        foreach($statuses as $status)
        {
            $row = [];
            $row['created_at'] = $status->created_at;
            $row['text'] = TwitterHelper::SanitizeLinks($status);
            $row['tweet_id'] = $status->id_str;
            $row['favourite_count'] = $status->favorite_count;
            $row['retweet_count'] = $status->retweet_count;
            if(isset($status->entities->media))
            {
                $media = $status->entities->media;
                if(count($media) > 0)
                {
                    $firstMedia = $media[0];
                    $row['media_type'] = $firstMedia->type;
                    $row['media_url'] = $firstMedia->media_url;
                }
            }
            $result[] = $row;
        }

        //$this->layout = '//layouts/column1';
        $this->layout='//layouts/adm_column1';
        $this->render('celeb', array('celeb' => $celeb, 'latestTweets' => $result));
    }

    public function actionChangeUsertype($id)
    {
        $user = TwitterUser::model()->find(array(
                    'condition' => 'screen_name=:screen_name',
                    'params' => array(':screen_name' => $id)
                )
            );
        if($user->usertype == TwitterUser::USERTYPE_NORMAL)
        {
            $user->usertype = TwitterUser::USERTYPE_CELEB;
            $user->save();
        }
        echo "Status updated successfully";
        Yii::app()->end();
    }
    
    public function actionSearch()
    {
        $searchTerm = Yii::app()->request->getPost('keyword');
        
        $criteria=new CDbCriteria;
        $criteria->select = array('screen_name');
        $criteria->addSearchCondition('screen_name', $searchTerm, TRUE, 'OR');
        $criteria->addSearchCondition('name', $searchTerm, TRUE, 'AND');
        
        /*$criteria->compare('screen_name', $searchTerm, TRUE, 'OR');
        $criteria->compare('name',$searchTerm, TRUE, 'OR');*/
        $criteria->limit = 5;
                
        $matchedAccounts = TwitterUser::model()->findAll($criteria);
        echo '<ul id="accounts-list">';
        foreach($matchedAccounts as $account) {
            ?><li onClick="selectCeleb('<?php echo $account["screen_name"]; ?>');"><?php echo $account["screen_name"]; ?></li><?php
        }
        echo '</ul>';
    }

    public function actionUpdateCountryConfig()
    {
        $countries = Country::model()
                    ->findAll(array(
                        'order' => 'id', 
                        'limit' => '6',
                        'condition' => 'id>46'
                    ));
            
        $accessToken = Yii::app()->session['access_token'];
        $twitter = Yii::app()->twitter->getTwitterTokened($accessToken['oauth_token'], 
                $accessToken['oauth_token_secret']);
        foreach ($countries as $country) {
            $result = (array)$twitter->get('geo/search', 
                array('granularity' => 'country', 'query' => $country['country_name'], 'trim_place' => TRUE));
            
            $places = $result['result']->places;
            foreach ($places as $place) {
                if($place->country_code == $country['country_code'])
                {
                    $centroid = $place->centroid;
                    $long = $centroid[0];
                    $lat = $centroid[1];

                    //get WOEID
                    $yahooId = (array)$twitter->get('trends/closest', 
                        array('long' => $long, 'lat' => $lat));
                    $woeid = $yahooId[0]->woeid;

                    $country->longitude = $long;
                    $country->latitude = $lat;
                    $country->woeid = $woeid;
                    echo $place->country . ";lat: " . $lat . ";long: " . $long . ";woeid: " . $woeid;
                    echo "<br />";
                    $country->save();
                }
            }
            
        }
        die();
    }
}
