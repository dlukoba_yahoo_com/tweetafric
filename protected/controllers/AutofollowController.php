<?php
class AutofollowController extends Controller
{
	/**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/main';
    /**
     * @return array action filters
     */
    public function filters()
    {
            return array(
                    'accessControl', // perform access control for CRUD operations
            );
    }
        
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                    'actions'=>array('autofollow'),
                    'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                    'actions'=>array('checkout', 'confirm'),
                    'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                    'actions'=>array('delete'),
                    'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                    'users'=>array('*'),
            ),
        );
    }

    public function actionAutofollow()
    {
//        $params = array('url' => 'http://abc.com', 'name' => 'sample name');
//        
//    	$mail = new YiiMailer('_mailInvite', $params);
//        $mail->clearLayout();
//        $mail->setFrom('daniel@lukoba.com', 'TweetAfric');
//        $mail->setTo("dlukoba@yahoo.com");
//        $mail->setSubject('Cron');
//        
//        $mail->send();
//        
//        Yii::app()->end();
//        
        //get top X users to be followed
        $users_query = "SELECT pe.id as expiryid, (pe.totalcustomersexpected - pe.customerssofar) AS customerdeficit, 
        tu.id as userid, tu.twitter_user_id, pt.followersperhour 
        FROM `bc_paymentexpiry` pe
        INNER JOIN `bc_twitter_user` tu ON pe.twitteruser_id = tu.id
        INNER JOIN `bc_paypalresponse` pr ON pe.paypalresponse_id = pr.id
        INNER JOIN `bc_paymentterm` pt ON pr.paymenttermref = pt.id
        WHERE pe.`isactive` = 1
        AND pe.`expirydate` > cast(now() as date)
        AND pe.customerssofar < pe.totalcustomersexpected
        ORDER BY tu.last_i_was_followed ASC
        LIMIT 0,5";
        $user_results = Yii::app()->db->createCommand($users_query)->queryAll();
        
        $user_ids = array();
        foreach ($user_results as $user_result) {
            $user_ids[] = $user_result['userid'];
        }
        
        //get categories for the users above
        $categories_query = "SELECT tuc.twitteruser_id, tuc.category_id FROM `bc_twitteruser_category` tuc
        WHERE tuc.twitteruser_id IN (:users)";
        $user_categories = Yii::app()->db->createCommand($categories_query)
                ->bindValue('users', implode(',', $user_ids))->queryAll();
        
        $cat_ids = array();
        foreach ($user_categories as $user_category) {
            $cat_ids[] = $user_category['category_id'];
        }
        
        //foreach user follow, match users in their categories and follow them
        //update # followed users
        foreach ($user_results as $user_result)
        {
            $fph = ($user_result['followersperhour'] < $user_result['customerdeficit']) 
                    ? $user_result['followersperhour'] : $user_result['customerdeficit'];
                    
            $users_to_follow_query = "SELECT DISTINCT tu.twitter_user_id, tu.id as userid,
            tu.oauth_token, tu.oauth_token_secret
            FROM `bc_twitteruser_category` tuc
            INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
            WHERE tuc.category_id IN (:categories)
            AND tu.id != :userid
            AND tu.usertype = 1
            AND (tu.next_follow_not_before IS NULL OR tu.next_follow_not_before < NOW())
			AND tu.oauth_token IS NOT NULL
            ORDER BY tu.friends_count ASC
            LIMIT 0, $fph";
            $users_to_follow = Yii::app()->db->createCommand($users_to_follow_query)->bindValues(array(
                ':categories' => implode(',', $cat_ids),
                ':userid' => $user_result['userid']
            ))->queryAll();
            
            foreach ($users_to_follow as $user_to_follow)
            {
                $twitter = Yii::app()->twitter->getTwitterTokened($user_to_follow['oauth_token'], 
                $user_to_follow['oauth_token_secret']);
                $result = $twitter->post('friendships/create', array('user_id' => $user_result['twitter_user_id']));
                $hasPostFailed = array_key_exists('errors', $result);
                
                //the friendship was successfully created
                if(!$hasPostFailed)
                {
                    //update payment expiry data
                    $expirySql = "UPDATE `bc_paymentexpiry` SET customerssofar = customerssofar + 1 WHERE id=:id";
                    Yii::app()->db->createCommand($expirySql)->execute(array(':id' => $user_result['expiryid']));

                    //update user last_followed_date
                    $lastFollowedSql = "UPDATE `bc_twitter_user` SET last_i_was_followed = NOW() WHERE id=:id";
                    Yii::app()->db->createCommand($lastFollowedSql)->execute(array(':id' => $user_result['userid']));

                    //update next follow date
                    $nextFollowDateSql = "UPDATE `bc_twitter_user` SET next_follow_not_before = NOW() + INTERVAL 2 HOUR WHERE twitter_user_id=:twitter_user_id";
                    Yii::app()->db->createCommand($nextFollowDateSql)->execute(array(':twitter_user_id' => $user_to_follow['twitter_user_id']));

                    $log = new Followinglog;
                    $log->followed_id = $user_result['userid'];
                    $log->follower_id = $user_to_follow['userid'];
                    $log->actiondate = date('Y-m-d H:i:s');
                    $log->save();
                }
            }
            
        }
    }
}
?>