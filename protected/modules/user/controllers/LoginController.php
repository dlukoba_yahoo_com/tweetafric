<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';


/**
	 * Displays the login page
	 */
	public function actionLogin1()
	{
		$model=new LoginForm;

		$this->layout='//layouts/login_soft';//login soft
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		// collect user input data  was LoginForm before
		if(isset($_POST['LoginForm']))
		{
			//print_r($_POST); die();
			$model->attributes= $_POST; //was $_POST['LoginForm']
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				$loginStatus = 1;
				$this->redirect(Yii::app()->user->returnUrl);
			}else{
				Yii::app()->user->setFlash('loginMessage','Wrong Username / Password');	
			}
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}


	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
			 $this->layout='//layouts/login_soft';//login soft
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['LoginForm']))
			{
				$model->attributes=$_POST;
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					Yii::app()->user->setFlash('loginMessage','Login Successful. Welcome!');
					$this->lastVisit();

					if (Yii::app()->getBaseUrl()."/index.php" === Yii::app()->user->returnUrl)
						$this->redirect(Yii::app()->controller->module->returnUrl);
					else
						$this->redirect(Yii::app()->user->returnUrl);
				}
				else{
                                	Yii::app()->user->setFlash('loginMessage','Wrong Username / Password');
                        	}

			}
			// display the login form
			$this->render('/user/login',array('model'=>$model));
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}
	
	private function lastVisit() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit_at = date('Y-m-d H:i:s');
		$lastVisit->save();
	}

}
