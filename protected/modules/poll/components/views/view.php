<?php $this->render('results', array('model' => $model)); ?>

<?php if ($userVote->id): ?>
  <p id="pollvote-<?php echo $userVote->id ?>">
    You voted: <strong><?php echo $userChoice->label ?></strong>.<br />
    
  </p>
<?php else: ?>
    <p><?php echo CHtml::link('Vote', array('/poll/poll/vote', 'id' => $model->id), array('id' => 'lnkVote')); ?></p>
<?php endif; ?>
<p><?php echo CHtml::link('View past polls', array('/poll/poll/index')); ?></p>