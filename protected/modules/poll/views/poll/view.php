<?php
/*$this->breadcrumbs=array(
  'Polls'=>array('index'),
  $model->title,
);

$this->menu=array(
  array('label'=>'List Polls', 'url'=>array('index')),
  array('label'=>'Create Poll', 'url'=>array('create')),
  array('label'=>'Update Poll', 'url'=>array('update', 'id'=>$model->id)),
  array('label'=>'Export Poll', 'url'=>array('export', 'id'=>$model->id)),
  array('label'=>'Delete Poll', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
  array('label'=>'Manage Polls', 'url'=>array('admin')),
);*/
?>
<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<div id="modalWindow" class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
<!--                        <div class="modal-footer">
                <button type="button" id="btnSubmit" class="btn blue">Save changes</button>
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>-->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<h1><?php echo CHtml::encode($model->title); ?></h1>

<?php if ($model->description): ?>
<p class="description"><?php echo CHtml::encode($model->description); ?></p>
<?php endif; ?>

<?php $this->renderPartial('_results', array('model' => $model)); ?>

<?php if ($userVote->id): ?>
  <p id="pollvote-<?php echo $userVote->id ?>">
    You voted: <strong><?php echo $userChoice->label ?></strong>.<br />
    <?php
      /*if ($userCanCancel) {
        echo CHtml::ajaxLink(
          'Cancel Vote',
          array('/poll/pollvote/delete', 'id' => $userVote->id, 'ajax' => TRUE),
          array(
            'type' => 'POST',
            'success' => 'js:function(){window.location.reload();}',
          ),
          array(
            'class' => 'cancel-vote',
            'confirm' => 'Are you sure you want to cancel your vote?'
          )
        );
      }*/
    ?>
  </p>
<?php else: ?>
  <p><?php echo CHtml::link('Vote', array('/poll/poll/vote', 'id' => $model->id), array('id' => 'lnkVote')); ?></p>
<?php endif; 
?>
<p><?php echo CHtml::link('View past polls', array('/poll/poll/index')); ?></p>
<?php
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/js/DashboardHelper.js', CClientScript::POS_END);

?>
