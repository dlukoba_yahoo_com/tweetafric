<?= TbHtml::pageHeader("Polls", ""); ?>
<?php 
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => TbHtml::GRID_TYPE_STRIPED,
    'dataProvider' => $dataProvider,
    'template' => "{items}",
    'columns' => array(
        array(
            'name' => 'title',
            'header' => 'Title'
        ),
        array(
            'name' => 'description',
            'header' => 'Description',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}'
        )
    ),
));

/*$this->widget('zii.widgets.grid.CGridView', array(
  'dataProvider'=>$dataProvider,
  //'itemView'=>'_view',
));*/ ?>
