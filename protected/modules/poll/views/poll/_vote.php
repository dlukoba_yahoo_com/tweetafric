<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'poll-form',
  'enableAjaxValidation'=>false,
)); ?>

  <?php echo $form->errorSummary($model); ?>

  <div class="form-group">
    <?php echo $form->labelEx($vote,'choice_id'); ?>
    <?php $template = '<div class="row-choice clearfix"><div class="form-radio">{input}</div><div class="form-label">{label}</div></div>'; ?>
    <?php echo $form->radioButtonList($vote,'choice_id',$choices,array('template'=>$template,'separator'=>'')); ?>
    <?php echo $form->error($vote,'choice_id'); ?>
  </div>
    <div class="modal-footer">
        <button type="button" id="btnSubmit" class="btn blue">Vote</button>
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
$js = <<<JS
    $("#btnSubmit").click(function(evt){
        $.ajax({
            method: "POST",
            url: Yii.app.createUrl('poll/poll/vote/id/{$model->id}'),
            data: $("#poll-form").serialize()
        }).done(function(result){
            alert(result);
            window.location.reload();
        });
    });
JS;
Yii::app()->clientScript->registerScript('pollVoteModal', $js, CClientScript::POS_END);
?>