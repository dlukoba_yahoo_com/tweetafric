<?php
?>
<html>
<head>
</head>
<body>
    <p><?= $name ?> has sent you an invitation to Tweetafric</p>
    <p>Tweetafric is a dynamic web portal, home to an amazing community of like minded individuals and brands looking to boost their following using Twitter.</p>
    <p>On Tweetafric, you get to connect with other amazing people such as yourself based on interests, location & trends. Users can create polls and get quick feedback on anything from a new product being launched to the latest trend happening, grow your follower base based on your country and of course customize your interests any time - The best part is as you interact and engage on Tweetafric,  you get rewarded by earning Tweetafric $ that are redeemable for all sorts of goodies!</p>
    <p>Obviously, the benefits are endless. We suggest wasting no time in logging on to get exploring. It will take less than 30 seconds of your time to sign up, after, sit back and watch the power of localized social media work for you and your business.</p>
    <p>Regards,</p>
    <p>
        <a href="<?= $url ?>" target="_blank" rel="nofollow"><b>Tweetafric</b></a>
    </p>
</body>
</html>