<?php
?>
<p>
    <?php echo $paymentTerm->name . " (" .$paymentTerm->amount . ")"; ?>
</p>
<!-- INFO: The post URL "checkout.php" is invoked when clicked on "Pay with PayPal" button.-->
<?php echo CHtml::beginForm(Yii::app()->baseUrl . '/payment/checkout/' . $paymentTerm->id, 'post', array('id' => 'frmCheckout')); ?>
    <div class="form-group">
        <input type='image' name='paypal_submit' id='paypal_submit' src='https://www.paypal.com/en_US/i/btn/btn_dg_pay_w_paypal.gif' border='0' align='top' alt='Pay with PayPal'/>
    </div>
    <input type="hidden" name="paymentTerm" value="<?php echo $paymentTerm->id; ?>" />
<?php echo CHtml::endForm(); ?>


<!-- Add Digital goods in-context experience. Ensure that this script is added before the closing of html body tag -->
<script src='https://www.paypalobjects.com/js/external/dg.js' type='text/javascript'></script>
<script type="text/javascript">

	var dg = new PAYPAL.apps.DGFlow(
	{
		trigger: 'paypal_submit',
		expType: 'instant'
		 //PayPal will decide the experience type for the buyer based on his/her 'Remember me on your computer' option.
	});

</script>
