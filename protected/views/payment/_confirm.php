<?php
$ack = strtoupper($resArray["ACK"]);
if( $ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING" )
{
    ?>
    <script type="text/javascript">
    alert("Payment Successful");
    // add relevant message above or remove the line if not required
    window.onload = function(){
        if(window.opener){
            window.close();
        }
        else{
            if(top.dg.isOpen() == true){
                top.dg.closeFlow();
                return true;
            }
        }                              
    };
    </script>
<?php
}
else
{
    //Display a user friendly Error on the page using any of the following error information returned by PayPal
    $ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
    $ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
    $ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
    $ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);

    echo "DoExpressCheckoutDetails API call failed. ";
    echo "Detailed Error Message: " . $ErrorLongMsg;
    echo "Short Error Message: " . $ErrorShortMsg;
    echo "Error Code: " . $ErrorCode;
    echo "Error Severity Code: " . $ErrorSeverityCode;
    ?>
    <script type="text/javascript">
    alert("Payment failed");
    // add relevant message above or remove the line if not required
    window.onload = function(){
        if(window.opener){
            window.close();
        }
        else{
            if(top.dg.isOpen() == true){
                top.dg.closeFlow();
                return true;
            }
        }                              
    };
    </script>
<?php
}
?>