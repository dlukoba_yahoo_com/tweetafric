<?php
$config = parse_ini_file('tweetafricApp.ini', true);
if ($config['env']<>'live') 
    defined('YII_DEBUG') or define('YII_DEBUG',false);
require_once dirname(__FILE__) . '/../components/helpers.php'; 
include dirname(__FILE__) . '/../vendor/autoload.php'; // composer autoload
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
$options = array(
    
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Tweetafric',
    'theme'=>'metronic',
    // preloading 'log' component
    'preload'=>array(
        'log',
        //'bootstrap',
        'mailgun',
        'phirehose',
        'advanced',
        'EJSUrlManager'
    ),
    
    'aliases' => array(
        'bootstrap' => realpath(dirname(__FILE__) . '/../extensions/bootstrap'),
        'vendor.twbs.bootstrap.dist' => realpath(__DIR__ . '/../extensions/bootstrap')
    ),

    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.yiiauth.components.*',
        'application.modules.hybridauth.models.*',
        'application.modules.hybridauth.components.*',
        'application.modules.hybridauth.controllers.*',
        'application.modules.poll.models.*',
        'application.modules.poll.components.*',
        'application.extensions.CAdvancedArBehavior',
        'ext.YiiMailer.YiiMailer',
        'bootstrap.behaviors.TbWidget',
        'bootstrap.components.TbApi',
        'bootstrap.form.*',
        'bootstrap.helpers.*',
        'bootstrap.widgets.*',
    ),

    'modules'=>array(
        'auth' => array(
            'strictMode' => true, // when enabled authorization items cannot be assigned children of the same type.
            'userClass' => 'User', // the name of the user model class.
            'userIdColumn' => 'id', // the name of the user id column.
            'userNameColumn' => 'username', // the name of the user name column.
            'appLayout' => 'application.views.layouts.main', // the layout used by the module.
            'viewDir' => null, // the path to view files to use with this module.
        ),
        'user'=>array(            
            'hash' => 'md5', # encrypting method (php hash function)
            'sendActivationMail' => true, # send activation email
            'loginNotActiv' => false, # allow access for non-activated users
            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,
            'autoLogin' => true, # automatically login from registration
            'registrationUrl' => array('/user/registration'), // registration path
            'recoveryUrl' => array('/user/recovery'),   // # recovery password path
            'loginUrl' => array('/user/login'), // login form path
            'returnUrl' => array('/account/index'), //page after login
            'returnLogoutUrl' => array('/'), // page after logout
        ),
        /**
         * hybridauth config
         */
        'hybridauth' => array(
            'baseUrl' => 'http://'. $_SERVER['SERVER_NAME'] . '/csites/tweetafric/app/index.php/hybridauth',
            'withYiiUser' => true, // Set to true if using yii-user
            "providers" => array (
                "OpenID" => array (
                    "enabled" => false
                ),

                "Yahoo" => array (
                    "enabled" => false
                ),

                "Google" => array (
                    "enabled" => false,
                    "keys"    => array ( "id" => "", "secret" => "" ),
                    "scope"   => "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email" , // optional
                ),

                "Facebook" => array (
                    "enabled" => true,
                    "keys"    => array ( "id" => "", "secret" => "" ),
                    "scope"   => "email,publish_stream",
                    "display" => ""
                ),
                "Live" => array (
                    "enabled" => false,
                    "keys"    => array ( "id" => "", "secret" => "" ),
                ),

                "Twitter" => array (
                    "enabled" => true,
                    "keys"    => array ( "key" => "fDdNxsvKvlW11tSa4dSsl2HAa", "secret" => "U7vsNPoB22cOp1xFJ6CZo5R2ZbAQH9DlgfbArcQduy0bC8Q7E6" )
                )
            ),
        ),
		
        // hybrid auth module
        'yiiauth'=>array(
            'userClass'=>'User', //the name of your Userclass
            'config'=>array(
                "base_url" => "http://yourdomain.com/hybridauth/", 
                "providers" => array ( 
                    "Facebook" => array ( 
                        "enabled" => true,
                        "keys"    => array ( "id" => "sample-id", "secret" => "sample-secret" ),
                        // A comma-separated list of permissions you want to request from the user. See the Facebook docs for a full list of available permissions: http://developers.facebook.com/docs/reference/api/permissions.
                        "scope"   => "email,user_about_me,user_website", 
                        // The display context to show the authentication page. Options are: page, popup, iframe, touch and wap. Read the Facebook docs for more details: http://developers.facebook.com/docs/reference/dialogs#display. Default: page
                        "display" => "popup" 
                    ),
                ),

                // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
                "debug_mode" => false,

                "debug_file" => "",
            ),
        ),
        'poll' => array(
            // Force users to vote before seeing results
            'forceVote' => FALSE,
            // Restrict anonymous votes by IP address, otherwise it's tied only to user_id 
            'ipRestrict' => FALSE,
            // Allow guests to cancel their votes if ipRestrict is enabled
            'allowGuestCancel' => FALSE
        )
    ),

    // application components
    'components'=>array(
        'paypal' =>array(
            'class' => 'ext.PaypalHelper',
            'API_Endpoint' => 'https://api-3t.sandbox.paypal.com/nvp',
            'PAYPAL_URL' => 'https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=',
            'PAYPAL_DG_URL' => 'https://www.sandbox.paypal.com/incontext?token=',
            'API_UserName' => 'dlukoba-facilitator_api1.yahoo.com',
            'API_Password' => 'SUQKVEP5QG2HXNEL',
            'API_Signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AnGzGuPGNjzy2NGTisx.xxbDoiHh',
            'RETURNURL' => "http://" . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . "/tweetafric/app/payment/confirm",
            'CANCELURL' => "http://" . $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . "/tweetafric/app/payment/cancel"
        ),
        'twitter' => array(
            'class' => 'ext.yiitwitteroauth.YiiTwitter',
            'consumer_key' => '',
            'consumer_secret' => '',
            'callback' => '',
        ),
        'format'=>array(
                'class'=>'ext.timeago.TimeagoFormatter',
        ),
        's3'=>array(
            'class'=>'ext.s3.ES3',
            'aKey'=>$config['aws_s3_access'], 
            'sKey'=>$config['aws_s3_secret'],
        ),  
        'phpThumb'=>array(
            'class'=>'ext.EPhpThumb.EPhpThumb',
            'options'=>array(),
        ),	  
        'session' => array(
                'timeout' => 86400,
        ),
        'user'=>array(
                // enable cookie-based authentication
                'allowAutoLogin'=>true,
                'autoRenewCookie' => true,
                'authTimeout' => 31557600,
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.TbApi',   
        ),
//        'bootstrap' => array(
//                'class' => 'ext.bootstrap.components.Bootstrap',
//                'responsiveCss' => true,
//        ),	
		
        'authManager' => array(
                'class'=>'auth.components.CachedDbAuthManager',
                'cachingDuration'=>3600,
                'behaviors' => array(
                        'auth' => array(
                                'class' => 'auth.components.AuthBehavior',
                                'admins'=>array('admin'), // users with full access
                        ), 
                ),
        ),	
        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'caseSensitive'=>false,
            'rules'=>array(
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                //'<controller:\w+>/<action:\w+>/<id:\w+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                '/hybridauth' => '/hybridauth',
                '' => 'site/index'
            ),
        ),
        'db'=>array(
            'connectionString' => 'mysql:host='.$config['mysql_host'].';dbname='.$config['mysql_db'],
            'emulatePrepare' => true,
            'username' => $config['mysql_un'],
            'password' => $config['mysql_pwd'],
            'charset' => 'utf8',
            'tablePrefix'=>$config['mysql_tbl_prefix'],
        ),
        /* */
        'user'=>array(
            // enable cookie-based authentication
            'class' => 'WebUser',
            'class' => 'auth.components.AuthWebUser',
            'loginUrl' => array('/twitter/connect')
        ),
        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        'curl' => array(
            'class' => 'ext.Curl',
            //'options' => array(),
        ),
        'EJSUrlManager' => array(
            'class' => 'ext.JSUrlManager.src.EJSUrlManager'
        ),
//        'Mailer' => array(
//            'class' => 'ext.yii-mail.YiiMail',
//            'transportType' => 'smtp',
//            'transportOptions' => array(
//                'host' => 'smtp.gmail.com',
//                'username' => 'daniel@lukoba.com',
//                'password' => '43ffstop',
//                'port' => '587',
//                'encryption' => 'tls'
//            ),
//            'viewPath' => 'application.views.mail'
//        )
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        'env'=> $config['env'],
        'base_root'=> $config['base_root'], // used for localhost
        'mail_domain'=> $config['mail_domain'],
        'superuser'=>$config['superuser'],
        'adminEmail'=>'admin@yourdomain.com',
        'supportEmail'=>$config['support_email'],
        'postsPerPage'=> $config['postsPerPage'],
        'twitter_stream'=> $config['twitter_stream'],
        'mailgun'=> array(
                'api_key'=> $config['mailgun_api_key'],
                'api_url' => $config['mailgun_api_url']
        ),
    ),
);

// configure logging
if ($config['env'] <> 'live') {
    $options['components']['fixture']=array(
            'class'=>'system.test.CDbFixtureManager',
    );
    $options['components']['log']=
        array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array('class'=>'CWebLogRoute',
                    'levels'=>'error,warning,info,trace',
                ),
            )
        );
    	  // enable gii
    $options['modules']['gii'] =
    // uncomment the following to enable the Gii tool
    // path to gii = /gii/default/login
    array(
        'class'=>'system.gii.GiiModule',
        'password'=>$config['gii_pwd'],
        'generatorPaths' => array(
            'bootstrap.gii'
        ),
        // If removed, Gii defaults to localhost only. Edit carefully to taste.
        'ipFilters'=>array('127.0.0.1','::1'),
    );    	  
} else {
    $options['components']['log']=
        array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array('class'=>'CFileLogRoute','levels'=>'error, warning, info')
            )
        );  
}
return $options;

