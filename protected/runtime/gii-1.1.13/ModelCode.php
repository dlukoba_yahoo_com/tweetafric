<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => 'bc_',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
);
