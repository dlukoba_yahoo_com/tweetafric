<?php
class UserConfigForm extends CFormModel
{
	public $country_id;
	public $category_ids = array();

	public function rules()
	{
		return array(
			// username and password are required
			array('country_id', 'required'),
			array('category_ids', 'validateCategories'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'country_id' => 'Country',
			'category_ids' => 'Interests'
		);
	}

	public function validateCategories()
	{
		if(count($this->category_ids) < 1)
			$this->adderror('category', 'Please select at least 1 interest');
	}
}
?>