<?php

/**
 * This is the model class for table "{{paymentterm}}".
 *
 * The followings are the available columns in table '{{paymentterm}}':
 * @property integer $id
 * @property string $name
 * @property string $amount
 * @property string $introduction
 * @property string $description
 * @property integer $duration
 * @property integer $followersperhour
 * @property integer $totalfollowers
 *
 * The followings are the available model relations:
 * @property Paypalresponse[] $paypalresponses
 */
class Paymentterm extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Paymentterm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{paymentterm}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, amount, introduction, description, duration, followersperhour, totalfollowers', 'required'),
			array('duration, followersperhour, totalfollowers', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>30),
			array('amount', 'length', 'max'=>8),
			array('introduction', 'length', 'max'=>100),
			array('description', 'length', 'max'=>600),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, amount, introduction, description, duration, followersperhour, totalfollowers', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'paypalresponses' => array(self::HAS_MANY, 'Paypalresponse', 'paymenttermref'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'amount' => 'Amount',
			'introduction' => 'Introduction',
			'description' => 'Description',
			'duration' => 'Duration',
			'followersperhour' => 'Followersperhour',
			'totalfollowers' => 'Totalfollowers',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('introduction',$this->introduction,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('followersperhour',$this->followersperhour);
		$criteria->compare('totalfollowers',$this->totalfollowers);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}