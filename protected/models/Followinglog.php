<?php

/**
 * This is the model class for table "{{followinglog}}".
 *
 * The followings are the available columns in table '{{followinglog}}':
 * @property integer $id
 * @property integer $followed_id
 * @property integer $follower_id
 * @property string $actiondate
 *
 * The followings are the available model relations:
 * @property TwitterUser $follower
 * @property TwitterUser $followed
 */
class Followinglog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Followinglog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{followinglog}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('followed_id, follower_id, actiondate', 'required'),
			array('followed_id, follower_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, followed_id, follower_id, actiondate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'follower' => array(self::BELONGS_TO, 'TwitterUser', 'follower_id'),
			'followed' => array(self::BELONGS_TO, 'TwitterUser', 'followed_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'followed_id' => 'Followed',
			'follower_id' => 'Follower',
			'actiondate' => 'Actiondate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('followed_id',$this->followed_id);
		$criteria->compare('follower_id',$this->follower_id);
		$criteria->compare('actiondate',$this->actiondate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}