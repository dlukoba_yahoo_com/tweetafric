<?php
class CelebForm extends CFormModel
{
	public $twitter_username;
	public $country_id;
	public $category_ids = array();

	public function rules()
	{
		return array(
			// username and password are required
			array('twitter_username, country_id', 'required'),
                    array('twitter_username', 'validateUsernameUniqueness'),
			array('category_ids', 'validateCategories'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'twitter_username'=>'Twitter username',
			'country_id' => 'Country',
			'category_ids' => 'Categories'
		);
	}
        
        public function validateUsernameUniqueness()
        {
            $user = TwitterUser::model()->find(array(
                    'condition' => 'screen_name=:screen_name',
                    'select' => array('usertype'),
                    'params' => array(':screen_name' => $this->twitter_username)
                )
            );
            if($user != NULL)
            {
                if($user['usertype'] == TwitterUser::USERTYPE_NORMAL)
                    $this->addError ('twitter_username', 'The user already exists. <a target="_blank" href="'. Yii::app()->createAbsoluteUrl('twitter/changeUsertype', array('id' => $this->twitter_username)).'">Consider changing his usertype in order to make him a celeb</a>');
                if($user['usertype'] == TwitterUser::USERTYPE_CELEB)
                    $this->addError ('twitter_username', 'The user already exists as a celeb');
            }
        }

        public function validateCategories()
	{
		if(count($this->category_ids) < 1)
			$this->adderror('category', 'Please select at least 1 category');
	}
}
?>