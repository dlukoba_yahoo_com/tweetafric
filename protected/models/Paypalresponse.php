<?php

/**
 * This is the model class for table "{{paypalresponse}}".
 *
 * The followings are the available columns in table '{{paypalresponse}}':
 * @property integer $id
 * @property string $transactionid
 * @property string $transactiontype
 * @property string $paymenttype
 * @property string $ordertime
 * @property string $transactionamount
 * @property string $currencycode
 * @property string $feeamount
 * @property string $taxamount
 * @property string $paymentstatus
 * @property string $pendingreason
 * @property integer $userid
 * @property integer $paymenttermref
 *
 * The followings are the available model relations:
 * @property Paymentterm $paymenttermref0
 * @property TwitterUser $user
 */
class Paypalresponse extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Paypalresponse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{paypalresponse}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transactionid, transactiontype, paymenttype, ordertime, transactionamount, currencycode, feeamount, taxamount, paymentstatus, userid, paymenttermref', 'required'),
			array('userid, paymenttermref', 'numerical', 'integerOnly'=>true),
			array('transactionid', 'length', 'max'=>50),
			array('transactiontype, currencycode', 'length', 'max'=>5),
			array('paymenttype', 'length', 'max'=>10),
			array('transactionamount', 'length', 'max'=>7),
			array('feeamount, taxamount', 'length', 'max'=>6),
			array('paymentstatus, pendingreason', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, transactionid, transactiontype, paymenttype, ordertime, transactionamount, currencycode, feeamount, taxamount, paymentstatus, pendingreason, userid, paymenttermref', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'paymenttermref0' => array(self::BELONGS_TO, 'Paymentterm', 'paymenttermref'),
			'user' => array(self::BELONGS_TO, 'TwitterUser', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'transactionid' => 'Transactionid',
			'transactiontype' => 'Transactiontype',
			'paymenttype' => 'Paymenttype',
			'ordertime' => 'Ordertime',
			'transactionamount' => 'Transactionamount',
			'currencycode' => 'Currencycode',
			'feeamount' => 'Feeamount',
			'taxamount' => 'Taxamount',
			'paymentstatus' => 'Paymentstatus',
			'pendingreason' => 'Pendingreason',
			'userid' => 'Userid',
			'paymenttermref' => 'Paymenttermref',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('transactionid',$this->transactionid,true);
		$criteria->compare('transactiontype',$this->transactiontype,true);
		$criteria->compare('paymenttype',$this->paymenttype,true);
		$criteria->compare('ordertime',$this->ordertime,true);
		$criteria->compare('transactionamount',$this->transactionamount,true);
		$criteria->compare('currencycode',$this->currencycode,true);
		$criteria->compare('feeamount',$this->feeamount,true);
		$criteria->compare('taxamount',$this->taxamount,true);
		$criteria->compare('paymentstatus',$this->paymentstatus,true);
		$criteria->compare('pendingreason',$this->pendingreason,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('paymenttermref',$this->paymenttermref);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}