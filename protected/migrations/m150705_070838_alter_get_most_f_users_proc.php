<?php

class m150705_070838_alter_get_most_f_users_proc extends CDbMigration
{
	public function up()
	{
            $sql = @"DROP PROCEDURE `get_most_followed_users_proc`;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_most_followed_users_proc`(IN `country_id` INT) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER
SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 1 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Sports

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 2 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Music

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 3 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Entertainment

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 5 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) News

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 6 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Technology

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 7 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Fashion

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 8 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Travel

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 9 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Television

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 10 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Government

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 11 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Business

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 12 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Blogging

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 13 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Internet

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 14 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Jobs

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 15 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) PublicSpeaking

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 16 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Nightlife

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 17 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Radio

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
  WHERE c.id = 18 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) FoodDrinks

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
   WHERE c.id = 19 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Photography

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
   WHERE c.id = 20 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Politics

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
   WHERE c.id = 21 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) HealthFitness

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count, tu.countryid FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 22 AND tu.usertype = 2 AND (tu.countryid = country_id OR tu.countryid = 53)
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) Education";
            $this->execute($sql);
	}

	public function down()
	{
		echo "m150705_070838_alter_get_most_f_users_proc does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}