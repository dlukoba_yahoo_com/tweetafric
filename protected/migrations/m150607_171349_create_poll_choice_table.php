<?php

class m150607_171349_create_poll_choice_table extends CDbMigration
{
    protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
    
    private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }
    
    public function up()
    {
        $this->setTable('poll_choice');
        $this->createTable($this->tableName, array(
                'id'=> 'pk',            
                'poll_id'=> 'int(11) NOT NULL',            
                'label'=> "varchar(255) NOT NULL DEFAULT ''",            
                "votes"=> "int(11) unsigned NOT NULL DEFAULT '0'",            
                "weight" => "int(11) NOT NULL DEFAULT '0'",
            ), $this->MySqlOptions
        );
        $this->addForeignKey('fk_poll_choice_poll', $this->tableName, 'poll_id', $this->tablePrefix.'poll', 'id', 'CASCADE', 'CASCADE');
    }

	public function down()
	{
		echo "m150607_171349_create_poll_choice_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}