<?php

class m150827_123325_create_paymentterms_table extends CDbMigration
{
	protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
    
    private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }

	public function up()
	{
		$this->setTable('paymentterm');
            $this->createTable($this->tableName, array(
                    'id' => 'pk',            
                    'name' => 'varchar(30) NOT NULL',
                    'amount' => 'decimal(8,2) not null',
                    'introduction' => 'varchar(100) NOT NULL',
                    'description' => 'varchar(600) not null',
                    'duration' => 'smallint(4) not null',
                    'followersperhour' => 'smallint(4) not null',
                    'totalfollowers' => 'smallint(5) not null'
                ), $this->MySqlOptions
            );
	}

	public function down()
	{
		echo "m150827_123325_create_paymentterms_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}