<?php

class m150609_163102_extend__twitter_user_table_with_countryid extends CDbMigration
{
	public function up()
	{
            $this->addColumn('bc_twitter_user', 'countryid', 'int(11) null');
	}

	public function down()
	{
		echo "m150609_163102_extend__twitter_user_table_with_countryid does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}