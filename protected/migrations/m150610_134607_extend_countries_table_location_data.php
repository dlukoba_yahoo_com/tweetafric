<?php

class m150610_134607_extend_countries_table_location_data extends CDbMigration
{
	public function up()
	{
		$this->addColumn('bc_countries', 'latitude', 'decimal(18,14) null');
		$this->addColumn('bc_countries', 'longitude', 'decimal(18,14) null');
		$this->addColumn('bc_countries', 'woeid', 'int(11) null');
	}

	public function down()
	{
		echo "m150610_134607_extend_countries_table_location_data does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}