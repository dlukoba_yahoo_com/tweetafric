<?php

class m150824_191752_create_paypalresponse_table extends CDbMigration
{
    protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
    
    private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }
    
	public function up()
	{
            $this->setTable('paypalresponse');
            $this->createTable($this->tableName, array(
                    'id' => 'pk',            
                    'transactionid' => 'varchar(50) NOT NULL',
                    'transactiontype' => 'varchar(5) not null',
                    'paymenttype' => 'varchar(10) NOT NULL',
                    'ordertime' => 'datetime not null',
                    'transactionamount' => 'decimal(7,2) not null',
                    'currencycode' => 'varchar(5) not null',
                    'feeamount' => 'decimal(6,2) not null',
                    'taxamount' => 'decimal(6,2) not null',
                    'paymentstatus' => 'varchar(30) not null',
                    'pendingreason' => 'varchar(30) null'
                ), $this->MySqlOptions
            );
	}

	public function down()
	{
		echo "m150824_191752_create_paypalresponse_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}