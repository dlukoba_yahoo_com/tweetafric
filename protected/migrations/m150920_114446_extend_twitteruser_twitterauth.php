<?php

class m150920_114446_extend_twitteruser_twitterauth extends CDbMigration
{
    protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
    
    private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }
    
	public function up()
	{
            $this->setTable('twitter_user');
            $this->addColumn('bc_twitter_user', 'oauth_token', 'varchar(100) null');
            $this->addColumn('bc_twitter_user', 'oauth_token_secret', 'varchar(100) null');
	}

	public function down()
	{
		echo "m150920_114446_extend_twitteruser_twitterauth does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}