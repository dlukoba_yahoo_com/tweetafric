<?php

class m150920_155626_create_following_log_table extends CDbMigration
{
    protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
    
    private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }
   
	public function up()
	{
            $this->setTable('followinglog');
        $this->createTable($this->tableName, array(
                'id' => 'pk',            
                'followed_id' => 'int(11) NOT NULL',
                'follower_id' => 'int(11) not null',
                'actiondate' => 'datetime NOT NULL',
            ), $this->MySqlOptions
        );
        $this->addForeignKey('fk_followinglog_user_id', $this->tableName, 'followed_id', $this->tablePrefix.'twitter_user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_followinglog_follower_user_id', $this->tableName, 'follower_id', $this->tablePrefix.'twitter_user', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		echo "m150920_155626_create_following_log_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}