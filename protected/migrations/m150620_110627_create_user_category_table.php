<?php

class m150620_110627_create_user_category_table extends CDbMigration
{
    protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
    
    private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }
    
	public function up()
	{
            $this->setTable('twitteruser_category');
            $this->createTable($this->tableName, array(
                    'id'=> 'pk',            
                    'twitteruser_id' => 'INT(11) NOT NULL',
                    'category_id' => 'INT(11) NOT NULL',
                    'isactive' => 'tinyint(1) NOT NULL default 1'
                ), $this->MySqlOptions
            );
            $this->addForeignKey('fk_twitteruser_category_twitter_user', $this->tableName, 'twitteruser_id', $this->tablePrefix.'twitter_user', 'id', 'CASCADE', 'CASCADE');
            $this->addForeignKey('fk_twitteruser_category_category', $this->tableName, 'category_id', $this->tablePrefix.'category', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		echo "m150620_110627_create_user_category_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}