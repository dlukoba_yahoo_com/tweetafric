<?php

class m150607_165128_create_poll_table extends CDbMigration
{
    protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
    
    private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }
    
    public function up()
    {
        $this->setTable('poll');
        $this->createTable($this->tableName, array(
                'id'=> 'pk',            
                'title'=> 'varchar(255) NOT NULL',            
                'description'=> 'longtext',            
                "status"=> "tinyint(1) NOT NULL DEFAULT 1",            
            ), $this->MySqlOptions
        );
    }

    public function down()
    {
            echo "m150607_165128_create_poll_table does not support migration down.\n";
            return false;
    }

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}