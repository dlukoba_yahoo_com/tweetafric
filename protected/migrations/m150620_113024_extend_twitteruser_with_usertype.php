<?php

class m150620_113024_extend_twitteruser_with_usertype extends CDbMigration
{
	public function up()
	{
            $this->addColumn('bc_twitter_user', 'usertype', 'tinyint(1) not null default 1');
	}

	public function down()
	{
		echo "m150620_113024_extend_twitteruser_with_usertype does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}