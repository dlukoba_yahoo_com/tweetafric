<?php

class m150529_200430_alter_twitteruser_table extends CDbMigration
{
	public function up()
	{
            $this->addColumn('bc_twitter_user', 'date_entry_recorded', 'datetime not null');
	}

	public function down()
	{
		echo "m150529_200430_alter_twitteruser_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}