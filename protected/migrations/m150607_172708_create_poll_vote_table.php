<?php

class m150607_172708_create_poll_vote_table extends CDbMigration
{
    protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
    
    private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }
    
    public function up()
    {
        $this->setTable('poll_vote');
        $this->createTable($this->tableName, array(
                'id'=> 'pk',            
                'poll_id'=> 'int(11) NOT NULL',            
                'choice_id'=> "int(11) NOT NULL",            
                "user_id"=> "int(11) NOT NULL DEFAULT '0'",            
                "ip_address" => "varchar(50) not null default ''",
                "timestamp" => "int(11) unsigned not null default '0'"
            ), $this->MySqlOptions
        );
        $this->addForeignKey('fk_poll__vote_poll', $this->tableName, 'poll_id', $this->tablePrefix.'poll', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_poll__vote_chocice', $this->tableName, 'choice_id', $this->tablePrefix.'poll_choice', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_poll__vote_user', $this->tableName, 'user_id', $this->tablePrefix.'twitter_user', 'id', 'CASCADE', 'CASCADE');
    }

	public function down()
	{
		echo "m150607_172708_create_poll_vote_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}