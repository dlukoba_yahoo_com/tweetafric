<?php

class m150620_085937_create_categories_table extends CDbMigration
{
    protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
    
    private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }
    
	public function up()
	{
            $this->setTable('category');
            $this->createTable($this->tableName, array(
                    'id'=> 'pk',            
                    'name'=> 'varchar(50) NOT NULL'  
                ), $this->MySqlOptions
            );
            $this->insert('bc_category', array('name' => 'sports'));
	    $this->insert('bc_category', array('name' => 'music'));
            $this->insert('bc_category', array('name' => 'entertainment'));
            $this->insert('bc_category', array('name' => 'funny'));
            $this->insert('bc_category', array('name' => 'news'));
            $this->insert('bc_category', array('name' => 'technology'));
            $this->insert('bc_category', array('name' => 'fashion'));
            $this->insert('bc_category', array('name' => 'travel'));
            $this->insert('bc_category', array('name' => 'television'));
            $this->insert('bc_category', array('name' => 'government'));
            $this->insert('bc_category', array('name' => 'business'));
	}

	public function down()
	{
		echo "m150620_085937_create_categories_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}