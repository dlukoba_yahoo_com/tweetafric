<?php

class m150630_160245_add_global_country extends CDbMigration
{
	public function up()
	{
            $this->insert('bc_countries', array('country_code' => 'GL', 'country_name' => 'Global', 'capital' => 'Global', 'continent' => 'GL'));
	}

	public function down()
	{
		echo "m150630_160245_add_global_country does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}