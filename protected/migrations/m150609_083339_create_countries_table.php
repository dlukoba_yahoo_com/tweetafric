<?php

class m150609_083339_create_countries_table extends CDbMigration
{
	protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
	
	private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }
	
	public function up()
	{
		$this->setTable('countries');
        $this->createTable($this->tableName, array(
                'id'=> 'pk',            
                "country_code"=> "char(3) NOT NULL",            
                'country_name'=> 'varchar(50) NOT NULL',            
                "capital"=> "varchar(30) DEFAULT NULL",   
				"continent" => "char(2) NOT NULL"
            ), $this->MySqlOptions
        );
		//$this->insertMultiple('bc_countries', array(
		$this->insert('bc_countries', array('country_code' => 'BF', 'country_name' => 'Burkina Faso', 'capital' => 'Ouagadougou', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'BI', 'country_name' => 'Burundi', 'capital' => 'Bujumbura', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'BJ', 'country_name' => 'Benin', 'capital' => 'Porto-Novo', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'BW', 'country_name' => 'Botswana', 'capital' => 'Gaborone', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'CD', 'country_name' => 'Democratic Republic of the Congo', 'capital' => 'Kinshasa', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'CF', 'country_name' => 'Central African Republic', 'capital' => 'Bangui', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'CG', 'country_name' => 'Republic of the Congo', 'capital' => 'Brazzaville', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'CI', 'country_name' => 'Ivory Coast', 'capital' => 'Yamoussoukro', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'CM', 'country_name' => 'Cameroon', 'capital' => 'Yaoundé', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'CV', 'country_name' => 'Cape Verde', 'capital' => 'Praia', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'DZ', 'country_name' => 'Algeria', 'capital' => 'Algiers', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'EH', 'country_name' => 'Western Sahara', 'capital' => 'El Aaiú', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'ER', 'country_name' => 'Eritrea', 'capital' => 'Asmara', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'ET', 'country_name' => 'Ethiopia', 'capital' => 'Addis Ababa', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'GA', 'country_name' => 'Gabon', 'capital' => 'Libreville', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'GH', 'country_name' => 'Ghana', 'capital' => 'Accra', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'GM', 'country_name' => 'Gambia', 'capital' => 'Banjul', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'GN', 'country_name' => 'Guinea', 'capital' => 'Conakry', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'GQ', 'country_name' => 'Equatorial Guinea', 'capital' => 'Malabo', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'GW', 'country_name' => 'Guinea-Bissau', 'capital' => 'Bissau', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'KE', 'country_name' => 'Kenya', 'capital' => 'Nairobi', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'LR', 'country_name' => 'Liberia', 'capital' => 'Monrovia', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'LS', 'country_name' => 'Lesotho', 'capital' => 'Maseru', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'LY', 'country_name' => 'Libya', 'capital' => 'Tripoli', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'MA', 'country_name' => 'Morocco', 'capital' => 'Rabat', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'MG', 'country_name' => 'Madagascar', 'capital' => 'Antananarivo', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'ML', 'country_name' => 'Mali', 'capital' => 'Bamako', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'MR', 'country_name' => 'Mauritania', 'capital' => 'Nouakchott', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'MU', 'country_name' => 'Mauritius', 'capital' => 'Port Louis', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'MW', 'country_name' => 'Malawi', 'capital' => 'Lilongwe', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'MZ', 'country_name' => 'Mozambique', 'capital' => 'Maputo', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'NA', 'country_name' => 'Namibia', 'capital' => 'Windhoek', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'NE', 'country_name' => 'Niger', 'capital' => 'Niamey', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'NG', 'country_name' => 'Nigeria', 'capital' => 'Abuja', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'RE', 'country_name' => 'Réunion', 'capital' => 'Saint-Denis', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'RW', 'country_name' => 'Rwanda', 'capital' => 'Kigali', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'SC', 'country_name' => 'Seychelles', 'capital' => 'Victoria', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'SD', 'country_name' => 'Sudan', 'capital' => 'Khartoum', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'SH', 'country_name' => 'Saint Helena', 'capital' => 'Jamestown', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'SL', 'country_name' => 'Sierra Leone', 'capital' => 'Freetown', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'SN', 'country_name' => 'Senegal', 'capital' => 'Dakar', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'SO', 'country_name' => 'Somalia', 'capital' => 'Mogadishu', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'SS', 'country_name' => 'South Sudan', 'capital' => 'Juba', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'ST', 'country_name' => 'São Tomé and Príncipe', 'capital' => 'São Tomé', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'SZ', 'country_name' => 'Swaziland', 'capital' => 'Mbabane', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'TD', 'country_name' => 'Chad', 'capital' => 'N\'Djamena', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'TG', 'country_name' => 'Togo', 'capital' => 'Lomé', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'TN', 'country_name' => 'Tunisia', 'capital' => 'Tunis', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'TZ', 'country_name' => 'Tanzania', 'capital' => 'Dodoma', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'ZA', 'country_name' => 'South Africa', 'capital' => 'Pretoria', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'ZM', 'country_name' => 'Zambia', 'capital' => 'Lusaka', 'continent' => 'AF'));
		$this->insert('bc_countries', array('country_code' => 'ZW', 'country_name' => 'Zimbabwe', 'capital' => 'Harare', 'continent' => 'AF'));
	}

	public function down()
	{
		echo "m150609_083339_create_countries_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}