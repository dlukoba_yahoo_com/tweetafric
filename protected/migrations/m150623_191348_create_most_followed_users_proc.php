<?php

class m150623_191348_create_most_followed_users_proc extends CDbMigration
{
	public function up()
	{
            $sql = @"DROP PROCEDURE IF EXISTS `get_most_followed_users_proc`;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_most_followed_users_proc`() NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER
SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 1
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) grp1

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 2
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) grp2

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 3
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) grp3

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 4
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) grp4

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 5
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) grp5

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 6
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) grp6

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 7
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) grp7

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 8
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) grp8

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 9
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) grp9

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 10
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) grp10

UNION ALL

SELECT * FROM 
(
    SELECT c.id AS category_id, c.name as category_name, tu.twitter_user_id, tu.name AS twitter_name, tu.screen_name, tu.profile_image_url, tu.followers_count FROM `bc_twitteruser_category` tuc
    INNER JOIN `bc_category` c ON tuc.category_id = c.id
    INNER JOIN `bc_twitter_user` tu ON tuc.twitteruser_id = tu.id
    WHERE c.id = 11
    ORDER BY tu.followers_count DESC
    LIMIT 0, 5
) grp11";
            $this->execute($sql);
	}

	public function down()
	{
		echo "m150623_191348_create_most_followed_users_proc does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}