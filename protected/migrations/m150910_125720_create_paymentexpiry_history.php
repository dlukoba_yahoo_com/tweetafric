<?php

class m150910_125720_create_paymentexpiry_history extends CDbMigration
{
	protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
    
    private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }

	public function up()
	{
		$this->setTable('paymentexpiry');
        $this->createTable($this->tableName, array(
                'id' => 'pk',            
                'twitteruser_id' => 'int(11) NOT NULL',
                'datecreated' => 'datetime not null',
                'expirydate' => 'date NOT NULL',
                'isactive' => 'tinyint(1) not null',
                'paypalresponse_id' => 'int(11) not null',
                'customerssofar' => 'smallint(5) not null',
                'totalcustomersexpected' => 'smallint(5) not null'
            ), $this->MySqlOptions
        );
        $this->addForeignKey('fk_paymentexpiry_twitter_user', $this->tableName, 'twitteruser_id', $this->tablePrefix.'twitter_user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_paymentexpiry_paypalresponse', $this->tableName, 'paypalresponse_id', $this->tablePrefix.'paypalresponse', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		echo "m150910_125720_create_paymentexpiry_history does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}