<?php

class m150827_125116_extend_paypalresponse_table extends CDbMigration
{
	protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
    
    private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }

	public function up()
	{
		$this->setTable('paypalresponse');
		$this->addColumn('bc_paypalresponse', 'userid', 'int not null');
		$this->addColumn('bc_paypalresponse', 'paymenttermref', 'int not null');
		$this->addForeignKey('fk_paypalresponse_twitter_user', $this->tableName, 'userid', $this->tablePrefix.'twitter_user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_paypalresponse_paymentterm', $this->tableName, 'paymenttermref', $this->tablePrefix.'paymentterm', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		echo "m150827_125116_extend_paypalresponse_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}