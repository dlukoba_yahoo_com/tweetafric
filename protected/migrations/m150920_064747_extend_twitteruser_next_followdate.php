<?php

class m150920_064747_extend_twitteruser_next_followdate extends CDbMigration
{
    protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci';
    private $tablePrefix;
    private $tableName;
    
    private function setTable($tblName) {
        $this->tablePrefix = Yii::app()->getDb()->tablePrefix;
        if($this->tablePrefix <> '')
            $this->tableName = $this->tablePrefix.$tblName;
    }
    
	public function up()
	{
            $this->setTable('twitter_user');
            $this->addColumn('bc_twitter_user', 'next_follow_not_before', 'datetime null');
	}

	public function down()
	{
		echo "m150920_064747_extend_twitteruser_next_followdate does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}