var fetchTimelineData = function(){
    postToUrl('POST', Yii.app.createUrl('twitter/fetchLatestItems'), {api: 'timeline'});
};

var fetchMentionsData = function(){
    postToUrl('POST', Yii.app.createUrl('twitter/fetchLatestItems'), {api: 'mentions'});
};

var fetchTrends = function(){
    $.ajax({
        method: "GET",
        url: Yii.app.createUrl('twitter/fetchTrends'),
        data: {}
    }).done(function(result){
        $(".task-list").replaceWith(result);
    });
}

function postToUrl(method, url, data)
{
    $.ajax({
        method: method,
        url: url,
        data: data
    }).done(function(result){
        if(data.api == 'timeline')
            $("#tab_1_1 ul.fetched_tweets").replaceWith(result);
        else
            $("#tab_1_2 ul.fetched_tweets").replaceWith(result);
    });
}

fetchTimelineData();
//fetch statuses every 1 minute == 60,000 secs
window.setInterval(fetchTimelineData, 75000);

window.setTimeout(fetchMentionsData(), 10000);
//fetch mentions every 2 mins
window.setInterval(fetchMentionsData, 120000);

fetchTrends();

function displayCountryUpdateForm(){
    var win = $("#modalWindow").modal("show");
    $(".modal-title").text("Please select your country and interests");
    win.find(".modal-body").load(Yii.app.createUrl('twitter/configureCountry'));
}

$("#lnkVote").click(function(evt){
   evt.preventDefault();
   var win = $("#modalWindow").modal("show");
    $(".modal-title").text("Vote");
    var href = $(this).attr('href');
    var id = href.substring(href.lastIndexOf('/') + 1);
    var url = Yii.app.createUrl('poll/poll/vote/id/' + id);
    win.find(".modal-body").load(url);
    $("#btnSubmit").text("Vote");
    $("#action").val(url);
});

$("#btnTweet").click(function(evt){
    evt.preventDefault();
    var win = $("#modalWindow").modal("show");
    $(".modal-title").text("Tweet");
    var url = Yii.app.createUrl('twitter/tweet');
    win.find(".modal-body").load(url);
    $("#btnSubmit").text("Tweet");
    //$("#action").val(url);
});

$(".in-reply-to").live("click", function(evt){
    evt.preventDefault();
    var tweet_id = $(this).attr('id');
    var screen_name = $(this).siblings('input[type=hidden]').val();
    var win = $("#modalWindow").modal("show");
    $(".modal-title").text("Reply to tweet");
    var url = Yii.app.createUrl('twitter/tweet?tweet_id='+ tweet_id +"&username="+ screen_name);
    win.find(".modal-body").load(url);
    $("#btnSubmit").text("Reply");
});

$(".retweet").live("click", function(evt){
    evt.preventDefault();
    $.ajax({
        method: "POST",
        url: Yii.app.createUrl('twitter/retweet'),
        data: {tweet_id: $(this).attr('id')}
    }).done(function(result){
        alert(result);
    });
});

$(".favorite").live("click", function(evt){
   evt.preventDefault();
   $.ajax({
        method: "POST",
        url: Yii.app.createUrl('twitter/favorite'),
        data: {tweet_id: $(this).attr('id')}
    }).done(function(result){
        alert(result);
    });
});

$(".account-settings").click(function(evt){
    evt.preventDefault();
    var win = $("#modalWindow").modal("show");
    $(".modal-title").text("Update your country and interests");
    var url = Yii.app.createUrl('twitter/configureCountry');
    win.find(".modal-body").load(url);
    $("#btnSubmit").text("Reply");
});

$("#btnFollow").click(function(){
    $.ajax({
        method: "POST",
        url: Yii.app.createUrl('twitter/followuser'),
        data: {user_to_follow: $("#user_id").val()}
    }).done(function(result){
        alert(result);
    });
});

$("#btnInvite").click(function(){
    var win = $("#modalWindow").modal("show");
    $(".modal-title").text("Invite your friend");
    var url = Yii.app.createUrl('twitter/invite');
    win.find(".modal-body").load(url);
});

//$("#searchQ").typeahead(null, {
//        name: 'searchQ',
//        remote: Yii.app.createUrl('twitter/search')
//    }
//);

$("#searchQ").keyup(function(){
    $.ajax({
        type: "POST",
        url: Yii.app.createUrl('twitter/search'),
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            //$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
        },
        success: function(data){
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(data);
            //$("#searchQ").css("background","#FFF");
        }
    });
});

function selectCeleb(val) {
    $("#searchQ").val(val);
    $("#suggesstion-box").hide();
}

$("#frmSearchCeleb").submit(function(e){
    e.preventDefault();
    var url = Yii.app.createUrl('twitter/celeb/' + $("#searchQ").val());
    $(location).attr('href', url);
});

