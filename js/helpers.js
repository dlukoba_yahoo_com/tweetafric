function selectText(element) {
    var doc = document
        , text = doc.getElementById(element)
        , range, selection
    ;    
    if (doc.body.createTextRange) { //ms
        range = doc.body.createTextRange();
        range.moveToElementText(text);
        range.select();
    } else if (window.getSelection) { //all others
        selection = window.getSelection();        
        range = doc.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
    }
}

$('.lnkFollow').click(function(evt){
    evt.preventDefault();
    postToUrl("POST", "followuser", {user_to_follow: $(this).attr('id')});
//    $.ajax({
//        beforeSend: function(){
//            $("#loading").show();
//        },
//        method: "POST",
//        url: "followuser",
//        data: {user_to_follow: $(this).attr('id')}
//    }).done(function(data){
//        alert(data);
//        $("#loading").hide();
//    });
});

function postToUrl(method, url, data)
{
    $.ajax({
        beforeSend: function(){
            $("#loading").show();
        },
        method: method,
        url: url,
        data: data
    }).done(function(data){
        alert(data);
        $("#loading").hide();
    });
}

$('.lnkUnFollow').click(function(evt){
    evt.preventDefault();
    postToUrl("POST", "unfollowuser", {user_to_unfollow: $(this).attr('id')});
    /*$.ajax({
        beforeSend: function(){
            $("#loading").show();
        },
        method: "POST",
        url: "unfollowuser",
        data: {user_to_unfollow: $(this).attr('id')}
    }).done(function(data){
        alert(data);
        $("#loading").hide();
    });*/
});
